package pt.cmfelgueiras.app_patrimonio.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import pt.cmfelgueiras.app_patrimonio.R;
import pt.cmfelgueiras.app_patrimonio.activities.DetailsActivity;
import pt.cmfelgueiras.app_patrimonio.adapters.RecyclerAdapter;
import pt.cmfelgueiras.app_patrimonio.classes.Route;

/**
 * A simple {@link Fragment} subclass.
 */
public class RouteFragment extends Fragment implements RecyclerAdapter.ListListener {

    private AppCompatActivity activity;
    private Context context;

    private List<Route> list;

    public RouteFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (AppCompatActivity) getActivity();
        this.context = context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_route, container, false);
        setListeners(v);
        RecyclerView recyclerView = v.findViewById(R.id.recycler_view);
        list = new ArrayList<>();
        for (int i = 0; i < 9; i++) {
            list.add(new Route(
                    "Rota Vinho Verde",
                    "1000",
                    "http://br.visitportoandnorth.travel/var/porto_norte/storage/images/po" +
                            "rto-and-the-north/visit/artigos/along-the-vinho-verde-route/447382-2" +
                            "-eng-US/Na-Rota-dos-Vinhos-Verdes.jpg", "16 KM"));
        }
        recyclerView.setAdapter(
                new RecyclerAdapter(list, RecyclerAdapter.TYPE_LIST_ROUTE, this));
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        return v;
    }

    private void setListeners(View view) {
        ImageView image = activity.findViewById(R.id.toolbar_icon);
        image.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_route_24dp));

        Button location = view.findViewById(R.id.btn_near_me);
        location.setSelected(true);
        location.setOnClickListener(button -> button.setSelected(!button.isSelected()));
        Button other = view.findViewById(R.id.gast);
        other.setOnClickListener(button -> button.setSelected(!button.isSelected()));
    }

    @Override
    public void onCardClick(View v, int i, String type) {
        Route item = list.get(i);
        Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra(DetailsActivity.KEY_TYPE, DetailsActivity.KEY_ROUTES_DETAILS);
        intent.putExtra(DetailsActivity.KEY_TITLE, item.getTitle());
        intent.putExtra(DetailsActivity.KEY_IMAGE, item.getImage());
        startActivity(intent);
    }
}
