package pt.cmfelgueiras.app_patrimonio.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import pt.cmfelgueiras.app_patrimonio.R;
import pt.cmfelgueiras.app_patrimonio.activities.DetailsActivity;
import pt.cmfelgueiras.app_patrimonio.adapters.RecyclerAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class AgendaFragment extends Fragment implements RecyclerAdapter.ListListener {

    public AppCompatActivity activity;
    public Context context;

    public AgendaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        this.activity = (AppCompatActivity) getActivity();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_agenda, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        List<CharSequence> list = new ArrayList<>();
        for (int i = 0; i < 9; i++) {
            list.add("Pinóquio - Um musical para sonhar");
        }
        recyclerView.setAdapter(
                new RecyclerAdapter(list, RecyclerAdapter.TYPE_LIST_AGENDA, this));
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        setDatePicker(view);

        Button nearMe = view.findViewById(R.id.btn_all);
        nearMe.setSelected(true);
        nearMe.setOnClickListener(v -> v.setSelected(!v.isSelected()));
        return view;
    }

    private void setDatePicker(View view) {
        final Calendar calendar = Calendar.getInstance(Locale.UK);
        final int currentYear = calendar.get(Calendar.YEAR);
        final int currentMonth = calendar.get(Calendar.MONTH);
        final int currentDay = calendar.get(Calendar.DAY_OF_MONTH);
        final String currentDate = currentDay + "/" + currentMonth + "/" + currentYear;

        TextView changeDate = view.findViewById(R.id.btn_change_date);
        changeDate.setText(currentDate);
        changeDate.setOnClickListener(v -> new DatePickerDialog(activity, (datePicker, year, month, dayOfMonth) -> {
            Calendar picked = Calendar.getInstance(Locale.UK);
            picked.set(year, month, dayOfMonth);
            if (calendar.before(picked)) {
                String date = dayOfMonth + "/" + (month + 1) + "/" + year;
                changeDate.setText(date);
            } else {
                Toast.makeText(activity, getString(R.string.invalid_date), Toast.LENGTH_SHORT).show();
            }
        }, currentYear, currentMonth, currentDay).show());

    }

    @Override
    public void onCardClick(View v, int i, String type) {
        Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra(DetailsActivity.KEY_TYPE, DetailsActivity.KEY_AGENDA_DETAILS);
        startActivity(intent);
    }
}
