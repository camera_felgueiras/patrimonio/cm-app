package pt.cmfelgueiras.app_patrimonio.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import pt.cmfelgueiras.app_patrimonio.R;
import pt.cmfelgueiras.app_patrimonio.activities.DetailsActivity;
import pt.cmfelgueiras.app_patrimonio.adapters.RecyclerAdapter;
import pt.cmfelgueiras.app_patrimonio.classes.Personality;
import pt.cmfelgueiras.app_patrimonio.dialogs.ErrorDialog;
import pt.cmfelgueiras.app_patrimonio.tasks.ApiTask;

/**
 * A simple {@link Fragment} subclass.
 */
public class PersonalitiesFragment extends Fragment implements RecyclerAdapter.ListListener, ApiTask.ApiListener {

    public AppCompatActivity activity;
    public Context context;
    private List<Personality> list;
    private ProgressBar pb;
    private RecyclerView recyclerView;

    public PersonalitiesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        this.activity = (AppCompatActivity) getActivity();
    }

    @Override
    public void onStart() {
        super.onStart();
        ApiTask task = new ApiTask();
        task.setListener(this);
        task.execute("GET", "/personality");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        pb = view.findViewById(R.id.pb);
        recyclerView = view.findViewById(R.id.recycler_view);
        return view;
    }

    @Override
    public void onCardClick(View v, int i, String type) {
        Personality item = list.get(i);
        Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra(DetailsActivity.KEY_TYPE, DetailsActivity.KEY_PERSONALITY_DETAILS);
        intent.putExtra(DetailsActivity.KEY_ID, item.getId());
        intent.putExtra(DetailsActivity.KEY_TITLE, item.getName());
        intent.putExtra(DetailsActivity.KEY_IMAGE_PERSONALITY, item.getImage());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onPre() {
        pb.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPost(JSONObject res) {
        JSONArray personalities;
        try {
            if (res.getInt("status_code") != 200) {
                String message = res.getString("message");
                ErrorDialog errorDialog = ErrorDialog.newInstance(message);
                errorDialog.show(getChildFragmentManager(), "error personalities list");
            } else {
                list = new ArrayList<>();
                personalities = res.getJSONArray("personalities");
                int personalitiesLength = personalities.length();
                for (int i = 0; i < personalitiesLength; i++) {
                    JSONObject obj = personalities.getJSONObject(i);
                    list.add(Personality.getFromJson(obj));
                }
                RecyclerAdapter adapter = new RecyclerAdapter(
                        list, RecyclerAdapter.TYPE_LIST_PERSONALITY, this);
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ErrorDialog errorDialog = new ErrorDialog();
            errorDialog.show(getChildFragmentManager(), "error personalities list");
        }
        pb.setVisibility(View.GONE);
    }
}
