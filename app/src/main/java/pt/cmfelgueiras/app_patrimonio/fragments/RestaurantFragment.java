package pt.cmfelgueiras.app_patrimonio.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import pt.cmfelgueiras.app_patrimonio.R;
import pt.cmfelgueiras.app_patrimonio.activities.DetailsActivity;
import pt.cmfelgueiras.app_patrimonio.adapters.RecyclerAdapter;
import pt.cmfelgueiras.app_patrimonio.classes.NaturalHeritageClass;

/**
 * A simple {@link Fragment} subclass.
 */
public class RestaurantFragment extends Fragment implements RecyclerAdapter.ListListener {

    public AppCompatActivity activity;
    public Context context;

    public RestaurantFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        this.activity = (AppCompatActivity) getActivity();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_restaurant, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        List<NaturalHeritageClass> list = new ArrayList<>();
        for (int i = 0; i < 9; i++) {
            list.add(new NaturalHeritageClass("3 Jorges", "1000", "http://www.portoenorte.pt/fot" +
                    "os/horeca/1459437925.9772_11717036505a61dd80b9401.jpg", "", new LatLng(41.3145093, -8.2139302)));
        }
        recyclerView.setAdapter(
                new RecyclerAdapter(list, RecyclerAdapter.TYPE_LIST_OLD, this));
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        Button nearMe = view.findViewById(R.id.btn_near_me);
        nearMe.setSelected(true);
        nearMe.setOnClickListener(v -> v.setSelected(!v.isSelected()));
        return view;
    }

    @Override
    public void onCardClick(View v, int i, String type) {
        Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra(DetailsActivity.KEY_TYPE, DetailsActivity.KEY_RESTAURANT_DETAILS);
        intent.putExtra(DetailsActivity.KEY_TITLE, "3 Jorges");
        intent.putExtra(DetailsActivity.KEY_IMAGE, "http://www.portoenorte.pt/fotos/horeca/1459437925.9772_11717036505a61dd80b9401.jpg");
        startActivity(intent);
    }
}
