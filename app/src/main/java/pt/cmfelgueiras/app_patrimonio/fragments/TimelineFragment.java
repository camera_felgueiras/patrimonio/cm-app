package pt.cmfelgueiras.app_patrimonio.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import pt.cmfelgueiras.app_patrimonio.R;
import pt.cmfelgueiras.app_patrimonio.adapters.DatesAdapter;
import pt.cmfelgueiras.app_patrimonio.adapters.TimelineAdapter;
import pt.cmfelgueiras.app_patrimonio.dialogs.YearPickerDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class TimelineFragment extends Fragment implements TimelineAdapter.Listener, DatesAdapter.Listener {

    private static final String TAG = TimelineFragment.class.getSimpleName();
    private static final int DATE_START = 1700;

    private Context context;
    private List<String> dateList;
    private DatesAdapter datesAdapter;
    private RecyclerView recyclerCards;

    public TimelineFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_timeline, container, false);

        dateList = new ArrayList<>();
        int currentDate = Calendar.getInstance().get(Calendar.YEAR);
        int finalDate = getDates(DATE_START, currentDate);
        RecyclerView recyclerDates = view.findViewById(R.id.recycler_dates);
        datesAdapter = new DatesAdapter(context, dateList, this);
        recyclerDates.setAdapter(datesAdapter);
        recyclerDates.setLayoutManager(new LinearLayoutManager(context));

        recyclerCards = view.findViewById(R.id.recycler_cards);

        TextView dateStart = view.findViewById(R.id.date_start);
        dateStart.setText(String.valueOf(DATE_START));
        TextView dateEnd = view.findViewById(R.id.date_end);
        dateEnd.setText(String.valueOf(finalDate));

        view.findViewById(R.id.btn_date_start).setOnClickListener(v -> {
            int startDate = Integer.valueOf(dateStart.getText().toString());
            int endDate = Integer.valueOf(dateEnd.getText().toString());
            showYearDialog(dateStart, startDate, DATE_START, endDate, true);
        });
        view.findViewById(R.id.btn_date_end).setOnClickListener(v -> {
            int startDate = Integer.valueOf(dateStart.getText().toString());
            int endDate = Integer.valueOf(dateEnd.getText().toString());
            showYearDialog(dateEnd, endDate, startDate, finalDate, false);
        });

        return view;
    }

    private void showYearDialog(TextView v, int currentSelected,
                                int minValue, int maxValue, boolean start) {
        YearPickerDialog dialog = YearPickerDialog.newInstance(currentSelected, minValue, maxValue);
        dialog.setListener(year -> {
            v.setText(String.valueOf(year));
            if (start) {
                getDates(year, maxValue);
            } else {
                getDates(minValue, year);
            }
            datesAdapter.notifyDataSetChanged();
        });
        dialog.show(getChildFragmentManager(), TAG);
    }

    private int getDates(int dateStart, int newDateEnd) {
        dateList.clear();
        int temp = dateStart;
        while (temp <= newDateEnd) {
            dateList.add(String.valueOf(temp));
            temp += 5;
        }
        return temp;
    }

    @Override
    public void onShareClick(int position) {
        Toast.makeText(context, "share", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLikeClick(int position) {
        Toast.makeText(context, "like", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCardClick(int position) {
        Toast.makeText(context, "card", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDateClick(int position, String date) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list.add(date);
        }
        recyclerCards.setAdapter(new TimelineAdapter(context, list, this));
        recyclerCards.setLayoutManager(new LinearLayoutManager(context));
    }
}
