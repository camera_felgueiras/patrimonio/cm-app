package pt.cmfelgueiras.app_patrimonio.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pt.cmfelgueiras.app_patrimonio.R;
import pt.cmfelgueiras.app_patrimonio.adapters.RelatedAdapter;
import pt.cmfelgueiras.app_patrimonio.classes.Related;
import pt.cmfelgueiras.app_patrimonio.utils.MenuUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class RouteDetailsFragment extends Fragment implements RelatedAdapter.RelatedListener {

    private AppCompatActivity activity;
    private Menu menu;
    private Context context;

    public RouteDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        this.activity = (AppCompatActivity) context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_route_details, container, false);
        setHasOptionsMenu(true);

        FloatingActionButton fab = activity.findViewById(R.id.fab);
        fab.setOnClickListener(v -> likeBehavior());

        AppBarLayout appBar = activity.findViewById(R.id.toolbar_collapse);
        appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {

            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    isShow = true;
                    MenuUtils.showOption(menu, R.id.menu_like);
                } else if (isShow) {
                    isShow = false;
                    MenuUtils.hideOption(menu, R.id.menu_like);
                }
            }
        });

        Bundle argsInfo = new Bundle();
        Bundle argsMap = new Bundle();
        Bundle argsContact = new Bundle(4);

        argsInfo.putString(InfoFragment.KEY_INFO, "O edíficio onde a escola funciona, que é " +
                "também conhecido por Casa do Curral, pertenceu à Câmara Municipal de Felgue" +
                "iras, sendo atualmente propriedade do Instituto Politécnico do Porto. Este " +
                "edifício fez parte do enredo de \"A Morgadinha dos Canaviais\" do escritor " +
                "Júlio Dinis. A Casa do Curral que em 1999 estava em estado de ruína, foi to" +
                "talmente reabilitada e revitalizada no ano de 2003 por projecto da autoria " +
                "do arquitecto português Filipe Oliveira Dias. ");

        argsMap.putBoolean(MapFragment.KEY_STATIC, true);
        argsMap.putDouble(MapFragment.KEY_LAT, 41.366849);
        argsMap.putDouble(MapFragment.KEY_LNG, -8.194727);

        argsContact.putString(ContactFragment.KEY_PHONE, "255314002");
        argsContact.putString(ContactFragment.KEY_EMAIL, "correio@estg.ipp.pt");
        argsContact.putString(ContactFragment.KEY_WEBSITE, "https://www.estg.ipp.pt");
        argsContact.putString(ContactFragment.KEY_LOCATION, "R. do Curral, 4610-156 Margaride (Santa Eulália)");

        view.findViewById(R.id.btn_info).setOnClickListener(v ->
                loadFragment(InfoFragment.newInstance(argsInfo)));
        view.findViewById(R.id.btn_path).setOnClickListener(v ->
                loadFragment(new PathFragment()));
        view.findViewById(R.id.btn_map).setOnClickListener(v -> {
            view.findViewById(R.id.frame_map).setVisibility(View.VISIBLE);
            loadFragment(MapFragment.newInstance(argsMap));
        });
        view.findViewById(R.id.btn_website).setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("https://duck.com"));
            startActivity(intent);
        });

        List<Related> related = new ArrayList<>();
        related.add(new Related(
                "nn",
                2,
                "Quinta da Maderne",
                "https://www.quintademaderne.com/wp-content/uploads/2016/06/quinta-photo_0" +
                        "01-1.jpg"));
        related.add(new Related(
                "nn",
                2,
                "Quinta da Lixa",
                "https://media-cdn.tripadvisor.com/media/photo-s/0d/42/a0/2b/getlstd-prope" +
                        "rty-photo.jpg"));
        related.add(new Related(
                "nn",
                2,
                "Quinta da Palmirinha",
                "https://scontent.flis7-1.fna.fbcdn.net/v/t1.0-0/q88/c55.2.390.390a/p526x3" +
                        "95/988899_665240480248672_6681097936175089736_n.jpg?_nc_cat=106&_nc_ht=s" +
                        "content.flis7-1.fna&oh=658e01aaf06b022823c92741a1bc6800&oe=5D5CA014"));

        RelatedAdapter relatedAdapter = new RelatedAdapter(related, this);
        RecyclerView recyclerRelated = view.findViewById(R.id.recycler_related);
        recyclerRelated.setAdapter(relatedAdapter);
        recyclerRelated.setLayoutManager(new LinearLayoutManager(
                context, LinearLayoutManager.HORIZONTAL, false));


        List<Related> eatList = new ArrayList<>();
        eatList.add(new Related(
                "nn",
                2,
                "Pizzaria BelBel",
                "https://media-cdn.tripadvisor.com/media/photo-s/0e/63/64/9c/photo0jpg.jpg"));
        eatList.add(new Related(
                "nn",
                2,
                "Restaurante Hede",
                "https://media-cdn.tripadvisor.com/media/photo-s/13/a0/09/36/le-hede-sur-l" +
                        "a-placette.jpg"));


        RelatedAdapter eatAdapter = new RelatedAdapter(eatList, this);
        RecyclerView recyclerEat = view.findViewById(R.id.recycler_eat);
        recyclerEat.setAdapter(eatAdapter);
        recyclerEat.setLayoutManager(new LinearLayoutManager(
                context, LinearLayoutManager.HORIZONTAL, false));

        List<Related> sleepList = new ArrayList<>();
        sleepList.add(new Related(
                "nn",
                2,
                "Hotel Horus",
                "https://r-cf.bstatic.com/images/hotel/max1024x768/345/34542046.jpg"));

        RelatedAdapter sleepAdapter = new RelatedAdapter(sleepList, this);
        RecyclerView recyclerSleep = view.findViewById(R.id.recycler_sleep);
        recyclerSleep.setAdapter(sleepAdapter);
        recyclerSleep.setLayoutManager(new LinearLayoutManager(
                context, LinearLayoutManager.HORIZONTAL, false));
        return view;
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        int id = R.id.frame_children_container;
        if (fragment instanceof MapFragment) {
            id = R.id.frame_map;
            swapViews(true);
        } else {
            swapViews(false);
        }
        transaction.replace(id, fragment);
        transaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.commit();
    }

    private void likeBehavior() {
        Toast.makeText(activity, "like", Toast.LENGTH_SHORT).show();
    }

    private void swapViews(boolean isMap) {
        if (getView() != null) {
            if (isMap) {
                getView().findViewById(R.id.frame_map).setVisibility(View.VISIBLE);
                getView().findViewById(R.id.frame_children_container).setVisibility(View.GONE);
            } else {
                getView().findViewById(R.id.frame_children_container).setVisibility(View.VISIBLE);
                getView().findViewById(R.id.frame_map).setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        this.menu = menu;
        inflater.inflate(R.menu.menu_fav_share, menu);
        MenuItem likeItem = menu.findItem(R.id.menu_like);
        MenuItem shareItem = menu.findItem(R.id.menu_share);
        likeItem.setIcon(MenuUtils.setIconColor(activity, likeItem));
        shareItem.setIcon(MenuUtils.setIconColor(activity, shareItem));
        MenuUtils.hideOption(menu, R.id.menu_like);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_share:
                Toast.makeText(activity, "share", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.menu_like:
                likeBehavior();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCardClick(View v, int pos, String type) {
        Toast.makeText(activity, "Card", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLikeClick(View v, int pos, String type) {
        Toast.makeText(activity, "Like", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onShareClick(View v, int pos, String type) {
        Toast.makeText(activity, "Share", Toast.LENGTH_SHORT).show();
    }
}
