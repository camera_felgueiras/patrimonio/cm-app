package pt.cmfelgueiras.app_patrimonio.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pt.cmfelgueiras.app_patrimonio.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AgendaDetailsFragment extends Fragment {

    private AppCompatActivity activity;

    public AgendaDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (AppCompatActivity) context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =
                inflater.inflate(R.layout.fragment_agenda_details, container, false);

        activity.findViewById(R.id.fab).setVisibility(View.GONE);

        CollapsingToolbarLayout collapsing = activity.findViewById(R.id.collapsing_layout);
        collapsing.setTitleEnabled(false);

        Bundle args = new Bundle();
        args.putString(InfoFragment.KEY_INFO, "António de Sousa Magalhães e Lemos, filho de João " +
                "António de Magalhães Lemos e de Emília de Jesus de Sousa, nasceu na Casa do Curr" +
                "al, Margaride, Felgueiras, no dia 18 de Agosto de 1855.\n Estudou nos liceus de " +
                "Braga e do Porto. Frequentou a Escola Médico-Cirúrgica do Porto onde se diplomou" +
                " com distinção em 17 de Outubro de 1882, após a apresentação e defesa da dissert" +
                "ação inaugural \"A Região Psychomotriz: apontamentos para contribuir ao estudo d" +
                "a sua anatomia\". O júri foi presidido por Ricardo Jorge e o trabalho dedicado a" +
                "os familiares próximos, ao corpo de catedráticos da Escola - sobretudo ao seu or" +
                "ientador, Pedro Augusto Dias -, ao amigo A. Plácido, a Costa Simões e aos seus c" +
                "ondiscípulos.\nA 28 de junho de 1883 foi nomeado, precedendo concurso, médico-aj" +
                "udante do recém-criado Hospital Conde de Ferreira e, mais tarde, a 12 de maio de" +
                " 1892, assumiu funções como médico-adjunto.\n A 15 de Fevereiro de 1887 foi apon" +
                "tado lente auxiliar das 7.ª, 8.ª, 9.ª, 10.ª e 11.ª cadeiras do Instituto Industr" +
                "ial e Comercial do Porto. Lecionou nesta instituição até 1929.\n Em 1889 editou " +
                "\"A paralysia geral\", dissertação de concurso à Escola Médico-Cirúrgica do Port" +
                "o.");
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, InfoFragment.newInstance(args));
        transaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.commit();

        return view;
    }
}
