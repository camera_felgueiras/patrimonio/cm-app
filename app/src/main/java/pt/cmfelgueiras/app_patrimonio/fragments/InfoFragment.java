package pt.cmfelgueiras.app_patrimonio.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import pt.cmfelgueiras.app_patrimonio.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class InfoFragment extends Fragment {
    public static final String KEY_INFO = "INFO";

    public AppCompatActivity activity;
    public Context context;

    public InfoFragment() {
        // Required empty public constructor
    }

    public static InfoFragment newInstance(Bundle args) {
        InfoFragment fragment = new InfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        this.activity = (AppCompatActivity) getActivity();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info, container, false);

        Bundle args = getArguments();
        if (args != null) {
            Button seeMoreLess = view.findViewById(R.id.btn_see_more_less);

            TextView textInfo = view.findViewById(R.id.text_info);

            String information = args.getString(KEY_INFO);
            if (information != null && information.length() > 255) {
                String shortInfo = information.substring(0, 255).concat("...");
                textInfo.setText(shortInfo);
                seeMoreLess.setOnClickListener(v -> {
                    if (textInfo.getText().equals(shortInfo)) {
                        seeMoreLess.setText(R.string.see_less);
                        textInfo.setText(information);
                    } else {
                        seeMoreLess.setText(R.string.see_more);
                        textInfo.setText(shortInfo);
                        Fragment parent = getParentFragment();
                        if (parent != null && parent.getView() != null) {
                            parent.getView().findViewById(R.id.focus).requestFocusFromTouch();
                        }
                    }
                });
            } else {
                textInfo.setText(information);
                seeMoreLess.setVisibility(View.GONE);
            }
        }
        return view;
    }
}

