package pt.cmfelgueiras.app_patrimonio.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pt.cmfelgueiras.app_patrimonio.R;
import pt.cmfelgueiras.app_patrimonio.adapters.RecyclerAdapter;
import pt.cmfelgueiras.app_patrimonio.dialogs.ErrorDialog;
import pt.cmfelgueiras.app_patrimonio.utils.LoadImagesUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class TicketLineFragment extends Fragment implements RecyclerAdapter.ListListener {

    private AppCompatActivity activity;
    private Context context;

    public TicketLineFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (AppCompatActivity) getActivity();
        this.context = context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_list, container, false);
        String message = getString(R.string.no_available);
        String title = getString(R.string.error);
        ErrorDialog errorDialog = ErrorDialog.newInstance(title, message);
        errorDialog.show(getChildFragmentManager(), "error TicketLine");
        /*
        setListeners();
        RecyclerView recyclerView = v.findViewById(R.id.recycler_view);
        List<CharSequence> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list.add("Not at Char " + i);
        }
        recyclerView.setAdapter(
                new RecyclerAdapter(list, RecyclerAdapter.TYPE_LIST_TICKETLINE, this));
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        */
        return v;
    }

    private void setListeners() {
        ImageView image = activity.findViewById(R.id.toolbar_icon);
        image.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_ticket_24dp));
    }

    @Override
    public void onCardClick(View v, int i, String type) {
        Toast.makeText(activity, "Card", Toast.LENGTH_SHORT).show();
    }
}
