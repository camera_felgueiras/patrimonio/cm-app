package pt.cmfelgueiras.app_patrimonio.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pt.cmfelgueiras.app_patrimonio.R;
import pt.cmfelgueiras.app_patrimonio.activities.DetailsActivity;
import pt.cmfelgueiras.app_patrimonio.adapters.RelatedAdapter;
import pt.cmfelgueiras.app_patrimonio.classes.Related;
import pt.cmfelgueiras.app_patrimonio.utils.MenuUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class MaterialHeritageDetailsFragment extends Fragment implements RelatedAdapter.RelatedListener {

    private AppCompatActivity activity;
    private Menu menu;
    private Context context;

    private List<Related> related;

    public MaterialHeritageDetailsFragment() {
        // Required empty public constructor
    }

    public static MaterialHeritageDetailsFragment newInstance(Bundle args) {
        MaterialHeritageDetailsFragment m = new MaterialHeritageDetailsFragment();
        m.setArguments(args);
        return m;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        this.activity = (AppCompatActivity) context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =
                inflater.inflate(R.layout.fragment_material_heritage, container, false);
        setHasOptionsMenu(true);

        FloatingActionButton fab = activity.findViewById(R.id.fab);
        fab.setOnClickListener(v -> likeBehavior());

        AppBarLayout appBar = activity.findViewById(R.id.toolbar_collapse);
        appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {

            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    isShow = true;
                    MenuUtils.showOption(menu, R.id.menu_like);
                } else if (isShow) {
                    isShow = false;
                    MenuUtils.hideOption(menu, R.id.menu_like);
                }
            }
        });

        Bundle args = getArguments();

        Bundle argsMap = new Bundle();
        Bundle argsContact = new Bundle(4);
        Bundle argsInfo = new Bundle();
        Bundle argsPhoto = new Bundle();

        related = new ArrayList<>();

        if (args.get("type").equals(DetailsActivity.KEY_HISTORICAL_HERITAGE_DETAILS)) {
            argsMap.putBoolean(MapFragment.KEY_STATIC, true);
            argsMap.putDouble(MapFragment.KEY_LAT, 41.366849);
            argsMap.putDouble(MapFragment.KEY_LNG, -8.194727);

            argsContact.putString(ContactFragment.KEY_PHONE, "255314002");
            argsContact.putString(ContactFragment.KEY_EMAIL, "correio@estg.ipp.pt");
            argsContact.putString(ContactFragment.KEY_WEBSITE, "https://www.estg.ipp.pt");
            argsContact.putString(ContactFragment.KEY_LOCATION, "R. do Curral, 4610-156 Margaride (Santa Eulália)");

            argsInfo.putString(InfoFragment.KEY_INFO, "O edíficio onde a escola funciona, que é " +
                    "também conhecido por Casa do Curral, pertenceu à Câmara Municipal de Felgue" +
                    "iras, sendo atualmente propriedade do Instituto Politécnico do Porto. Este " +
                    "edifício fez parte do enredo de \"A Morgadinha dos Canaviais\" do escritor " +
                    "Júlio Dinis. A Casa do Curral que em 1999 estava em estado de ruína, foi to" +
                    "talmente reabilitada e revitalizada no ano de 2003 por projecto da autoria " +
                    "do arquitecto português Filipe Oliveira Dias. ");

            ArrayList<String> photos = new ArrayList<>();
            photos.add("http://www.expressofelgueiras.com/wp-content/uploads/2017/03/ESTG-aspeto" +
                    "-geral.jpg");
            photos.add("http://www.expressofelgueiras.com/wp-content/uploads/2018/09/Escola-Supe" +
                    "rior-de-Tecnologia-de-Gest%C3%A3o-Felgueiras-ESTGF.jpg");
            photos.add("https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.ipp.pt%2Fnoticias%2" +
                    "Ftecnologicas-peakit-e-petapilot-instalam-centro-em-felgueiras%2Fimage_larg" +
                    "e&f=1");
            argsPhoto.putStringArrayList(PhotoFragment.KEY_PHOTOS, photos);

            related.add(new Related(
                    "p",
                    2,
                    "António de Sousa Magalhães Lemos",
                    "https://sigarra.up.pt/up/pt/web_gessi_docs.download_file?p_name=F-162" +
                            "6265777/Foto_Magalhaes_Lemos.png"));

            related.add(new Related(
                    "p",
                    2,
                    "António de Sousa Magalhães Lemos",
                    "https://sigarra.up.pt/up/pt/web_gessi_docs.download_file?p_name=F-162" +
                            "6265777/Foto_Magalhaes_Lemos.png"));

            related.add(new Related(
                    "p",
                    2,
                    "António de Sousa Magalhães Lemos",
                    "https://sigarra.up.pt/up/pt/web_gessi_docs.download_file?p_name=F-162" +
                            "6265777/Foto_Magalhaes_Lemos.png"));

            related.add(new Related(
                    "p",
                    2,
                    "António de Sousa Magalhães Lemos",
                    "https://sigarra.up.pt/up/pt/web_gessi_docs.download_file?p_name=F-162" +
                            "6265777/Foto_Magalhaes_Lemos.png"));

            related.add(new Related(
                    "p",
                    2,
                    "António de Sousa Magalhães Lemos",
                    "https://sigarra.up.pt/up/pt/web_gessi_docs.download_file?p_name=F-162" +
                            "6265777/Foto_Magalhaes_Lemos.png"));

        } else if (args.get("type").equals(DetailsActivity.KEY_NATURE_DETAILS)) {
            argsMap.putBoolean(MapFragment.KEY_STATIC, true);
            argsMap.putDouble(MapFragment.KEY_LAT, 41.3755724);
            argsMap.putDouble(MapFragment.KEY_LNG, -8.1981483);

            argsContact.putString(ContactFragment.KEY_PHONE, "NONE");
            argsContact.putString(ContactFragment.KEY_EMAIL, "NONE");
            argsContact.putString(ContactFragment.KEY_WEBSITE, "NONE");
            argsContact.putString(ContactFragment.KEY_LOCATION, "Alameda de Santa Quitéria " +
                    "4610, Felgueiras");

            argsInfo.putString(InfoFragment.KEY_INFO, "Situado bem no alto do Monte de Santa Qui" +
                    "téria, a partir deste miradouro vislumbra-se a cidade de Felgueiras. Tem ja" +
                    "rdins, parque infantil, café e restaurante.");

            ArrayList<String> photos = new ArrayList<>();
            photos.add("http://www.portoenorte.pt/fotos/oquefazer/1459437917.8304_19282564915a61" +
                    "dd4332ea4.jpg");
            photos.add("https://www.rotadoromanico.com/Galeria/Fotografias/Natureza%20e%20paisag" +
                    "em/MiradourodeSantaQuiteria_Felgueiras.jpg");
            photos.add("http://www.portoenorte.pt/fotos/oquefazer_imagens/1459437918.2163_326232" +
                    "0235a61dd45a1a89.jpg");
            argsPhoto.putStringArrayList(PhotoFragment.KEY_PHOTOS, photos);

            related.add(new Related("n", 2, "Santuario Sta. Quiteria", "https://lifecooler.c" +
                    "om/files/registos/imagens/349201/5961.jpg"));
        } else if (args.get("type").equals(DetailsActivity.KEY_RESTAURANT_DETAILS)) {
            argsMap.putBoolean(MapFragment.KEY_STATIC, true);
            argsMap.putDouble(MapFragment.KEY_LAT, 41.3145093);
            argsMap.putDouble(MapFragment.KEY_LNG, -8.2139302);

            argsContact.putString(ContactFragment.KEY_PHONE, "255934278");
            argsContact.putString(ContactFragment.KEY_EMAIL, "NONE");
            argsContact.putString(ContactFragment.KEY_WEBSITE, "NONE");
            argsContact.putString(ContactFragment.KEY_LOCATION, "Alameda de Santa Quitéria " +
                    "4610, Felgueiras");

            argsInfo.putString(InfoFragment.KEY_INFO, "O restaurante 3 jorges é muinto fixe");

            ArrayList<String> photos = new ArrayList<>();
            photos.add("http://www.portoenorte.pt/fotos/horeca/1459437925.9772_11717036505a61dd80b9401.jpg");
            argsPhoto.putStringArrayList(PhotoFragment.KEY_PHOTOS, photos);

            related.add(new Related("nn", 2, "Char at", "http://wiki.tripwirei" +
                    "nteractive.com/images/4/47/Placeholder.png"));
        } else if (args.get("type").equals(DetailsActivity.KEY_LODGING_DETAILS)) {
            argsMap.putBoolean(MapFragment.KEY_STATIC, true);
            argsMap.putDouble(MapFragment.KEY_LAT, 41.3768152);
            argsMap.putDouble(MapFragment.KEY_LNG, -8.2504896);

            argsContact.putString(ContactFragment.KEY_PHONE, "255934278");
            argsContact.putString(ContactFragment.KEY_EMAIL, "NONE");
            argsContact.putString(ContactFragment.KEY_WEBSITE, "NONE");
            argsContact.putString(ContactFragment.KEY_LOCATION, "Alameda de Santa Quitéria " +
                    "4610, Felgueiras");

            argsInfo.putString(InfoFragment.KEY_INFO, "O Alojamento é impacavel");

            ArrayList<String> photos = new ArrayList<>();
            photos.add("https://a0.muscache.com/im/pictures/328eeca2-a2b8-44fa-a64e-9cab73d857d5.jpg?aki_policy=x_large");
            argsPhoto.putStringArrayList(PhotoFragment.KEY_PHOTOS, photos);

            related.add(new Related("nn", 2, "Char at", "http://wiki.tripwirei" +
                    "nteractive.com/images/4/47/Placeholder.png"));
        }

        view.findViewById(R.id.btn_info).setOnClickListener(v ->
                loadFragment(InfoFragment.newInstance(argsInfo)));

        view.findViewById(R.id.btn_map).setOnClickListener(v -> {
            view.findViewById(R.id.frame_map).setVisibility(View.VISIBLE);
            loadFragment(MapFragment.newInstance(argsMap));
        });

        view.findViewById(R.id.btn_photos).setOnClickListener(v ->
                loadFragment(PhotoFragment.newInstance(argsPhoto)));

        view.findViewById(R.id.btn_contacts).setOnClickListener(v -> {
            loadFragment(ContactFragment.newInstance(argsContact));
        });

        RelatedAdapter relatedAdapter = new RelatedAdapter(related, this);
        RecyclerView recyclerRelated = view.findViewById(R.id.recycler_related);
        recyclerRelated.setAdapter(relatedAdapter);
        recyclerRelated.setLayoutManager(new LinearLayoutManager(
                context, LinearLayoutManager.HORIZONTAL, false));

        List<Related> eatList = new ArrayList<>();
        eatList.add(new Related(
                "nn",
                2,
                "Pizzaria BelBel",
                "https://media-cdn.tripadvisor.com/media/photo-s/0e/63/64/9c/photo0jpg.jpg"));
        eatList.add(new Related(
                "nn",
                2,
                "Restaurante Hede",
                "https://media-cdn.tripadvisor.com/media/photo-s/13/a0/09/36/le-hede-sur-l" +
                        "a-placette.jpg"));

        RelatedAdapter eatAdapter = new RelatedAdapter(eatList, this);
        RecyclerView recyclerEat = view.findViewById(R.id.recycler_eat);
        recyclerEat.setAdapter(eatAdapter);
        recyclerEat.setLayoutManager(new LinearLayoutManager(
                context, LinearLayoutManager.HORIZONTAL, false));

        List<Related> sleepList = new ArrayList<>();
        sleepList.add(new Related(
                "nn",
                2,
                "Hotel Horus",
                "https://r-cf.bstatic.com/images/hotel/max1024x768/345/34542046.jpg"));

        RelatedAdapter sleepAdapter = new RelatedAdapter(sleepList, this);
        RecyclerView recyclerSleep = view.findViewById(R.id.recycler_sleep);
        recyclerSleep.setAdapter(sleepAdapter);
        recyclerSleep.setLayoutManager(new LinearLayoutManager(
                context, LinearLayoutManager.HORIZONTAL, false));

        return view;
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        int id = R.id.frame_children_container;
        if (fragment instanceof MapFragment) {
            id = R.id.frame_map;
            swapViews(true);
        } else {
            swapViews(false);
        }
        transaction.replace(id, fragment);
        transaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.commit();
    }

    private void likeBehavior() {
        Toast.makeText(activity, "like", Toast.LENGTH_SHORT).show();
    }

    private void swapViews(boolean isMap) {
        if (getView() != null) {
            if (isMap) {
                getView().findViewById(R.id.frame_map).setVisibility(View.VISIBLE);
                getView().findViewById(R.id.frame_children_container).setVisibility(View.GONE);
            } else {
                getView().findViewById(R.id.frame_children_container).setVisibility(View.VISIBLE);
                getView().findViewById(R.id.frame_map).setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        this.menu = menu;
        inflater.inflate(R.menu.menu_fav_share, menu);
        MenuItem likeItem = menu.findItem(R.id.menu_like);
        MenuItem shareItem = menu.findItem(R.id.menu_share);
        likeItem.setIcon(MenuUtils.setIconColor(activity, likeItem));
        shareItem.setIcon(MenuUtils.setIconColor(activity, shareItem));
        MenuUtils.hideOption(menu, R.id.menu_like);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_share:
                Toast.makeText(activity, "share", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.menu_like:
                likeBehavior();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCardClick(View v, int i, String type) {
        if (type.equals("p")) {
            Related item = related.get(i);
            Intent intent = new Intent(context, DetailsActivity.class);
            intent.putExtra(DetailsActivity.KEY_TYPE, DetailsActivity.KEY_PERSONALITY_DETAILS);
            intent.putExtra(DetailsActivity.KEY_TITLE, item.getTitle());
            intent.putExtra(DetailsActivity.KEY_IMAGE,
                    "http://www.expressofelgueiras.com/wp-content/uploads/2017/03/ESTG-aspeto-geral.jpg");
            intent.putExtra(DetailsActivity.KEY_IMAGE_PERSONALITY, item.getImage());
            startActivity(intent);
        } else {
            Toast.makeText(activity, "Card", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onLikeClick(View v, int pos, String type) {
        Toast.makeText(activity, "Like", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onShareClick(View v, int pos, String type) {
        Toast.makeText(activity, "Share", Toast.LENGTH_SHORT).show();
    }
}
