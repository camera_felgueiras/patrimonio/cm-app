package pt.cmfelgueiras.app_patrimonio.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import pt.cmfelgueiras.app_patrimonio.BuildConfig;
import pt.cmfelgueiras.app_patrimonio.R;
import pt.cmfelgueiras.app_patrimonio.adapters.PhotoAdapter;
import pt.cmfelgueiras.app_patrimonio.dialogs.ImageViewDialog;
import pt.cmfelgueiras.app_patrimonio.tasks.ApiTask;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhotoFragment extends Fragment implements PhotoAdapter.PhotoListener {

    public static final String KEY_PHOTOS = "PHOTOS";
    public static final String KEY_TYPE = "FULL";

    private static final int PERMISSION_REQUEST_CODE = 100;
    public AppCompatActivity activity;
    public Context context;

    private List<String> list;
    private List<String> thList;

    public PhotoFragment() {
        // Required empty public constructor
    }

    public static PhotoFragment newInstance(Bundle args) {
        PhotoFragment fragment = new PhotoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        this.activity = (AppCompatActivity) getActivity();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo, container, false);

        Bundle args = getArguments();
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        if (args != null) {
            list = args.getStringArrayList(KEY_PHOTOS);
            thList = list;
            String type = args.getString(KEY_TYPE);
            if (type != null) {
                ArrayList<String> images = new ArrayList<>();
                int size = list.size();
                for (int i = 0; i < size; i++) {
                    String url = ApiTask.IMG_TH_URL + list.get(i);
                    images.add(url);
                }
                thList = images;
            }
        }
        recyclerView.setAdapter(new
                PhotoAdapter(thList, this));
        recyclerView.setLayoutManager(new
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        return view;
    }

    @Override
    public void onCardClick(View v, int pos) {
        String url = list.get(pos);
        ImageViewDialog dialog = ImageViewDialog.newInstance(url);
        dialog.show(getChildFragmentManager(), "image on big");
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ActivityCompat
                .checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this.activity,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_CODE);
        }
    }
}
