package pt.cmfelgueiras.app_patrimonio.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import pt.cmfelgueiras.app_patrimonio.R;
import pt.cmfelgueiras.app_patrimonio.activities.MapActivity;
import pt.cmfelgueiras.app_patrimonio.utils.PermissionUtils;

public class MapFragment extends SupportMapFragment
        implements OnMapReadyCallback, LocationListener, GoogleMap.OnMapLongClickListener {

    public static final String KEY_STATIC = "STATIC";
    public static final String KEY_TYPE = "TYPE";
    public static final String KEY_LAT = "LAT";
    public static final String KEY_LNG = "LNG";

    private AppCompatActivity activity;
    private Context context;
    private LocationManager manager;
    private GoogleMap map;
    private double lat, lng;

    public MapFragment() {
        // Required empty public constructor
    }

    public static MapFragment newInstance(Bundle bundle) {
        MapFragment map = new MapFragment();
        map.setArguments(bundle);
        return map;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (AppCompatActivity) getActivity();
        this.context = context;
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        super.getMapAsync(this);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        manager = (LocationManager) this.context
                .getSystemService(Context.LOCATION_SERVICE);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!PermissionUtils.checkPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)) {
            PermissionUtils.requestPermission(
                    this.activity, Manifest.permission.ACCESS_FINE_LOCATION);
            return;
        }
        manager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER, 0, 0, this);
    }

    @Override
    public void onStop() {
        super.onStop();
        manager.removeUpdates(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        int fineLocation = ActivityCompat
                .checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
        if (fineLocation != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        boolean isStatic = false;
        if (getArguments() != null) {
            isStatic = getArguments().getBoolean(KEY_STATIC, false);
            int type = getArguments().getInt(KEY_TYPE, -1);
            if (type == -1) {
                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            } else {
                map.setMapType(type);
            }
            lat = getArguments().getDouble(KEY_LAT);
            lng = getArguments().getDouble(KEY_LNG);
        }

        if (isStatic) {
            map.getUiSettings().setAllGesturesEnabled(false);
            map.setOnMapClickListener(latLng -> {
                Intent i = new Intent(context, MapActivity.class);
                i.putExtra(MapActivity.KEY_TYPE, map.getMapType());
                i.putExtra(MapActivity.KEY_LAT, lat);
                i.putExtra(MapActivity.KEY_LNG, lng);
                startActivity(i);
            });
        }

        map.setMyLocationEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(true);
        map.setBuildingsEnabled(true);
        map.setOnMapLongClickListener(this);
        LatLng latLng = new LatLng(lat, lng);
        map.addMarker(new MarkerOptions().position(latLng));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(getString(R.string.map_type));
        CharSequence[] options = new CharSequence[]{
                getString(R.string.map_type_normal),
                getString(R.string.map_type_satellite),
                getString(R.string.map_type_terrain)
        };
        builder.setItems(options, (dialog, which) -> {
            switch (which) {
                case 0:
                    map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    break;
                case 1:
                    map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                    break;
                case 2:
                    map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                    break;
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
