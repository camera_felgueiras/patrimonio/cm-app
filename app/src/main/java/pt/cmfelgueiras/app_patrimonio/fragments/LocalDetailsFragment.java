package pt.cmfelgueiras.app_patrimonio.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import pt.cmfelgueiras.app_patrimonio.R;
import pt.cmfelgueiras.app_patrimonio.activities.DetailsActivity;
import pt.cmfelgueiras.app_patrimonio.adapters.RelatedAdapter;
import pt.cmfelgueiras.app_patrimonio.classes.Contact;
import pt.cmfelgueiras.app_patrimonio.classes.Local;
import pt.cmfelgueiras.app_patrimonio.classes.Related;
import pt.cmfelgueiras.app_patrimonio.dialogs.ErrorDialog;
import pt.cmfelgueiras.app_patrimonio.tasks.ApiTask;
import pt.cmfelgueiras.app_patrimonio.utils.MenuUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class LocalDetailsFragment extends Fragment implements RelatedAdapter.RelatedListener, ApiTask.ApiListener {

    public static final String KEY_ID = "ID";

    private AppCompatActivity activity;
    private Menu menu;
    private Context context;

    private ProgressBar pb;
    private Bundle argsInfo, argsMap, argsPhotos, argsContacts;
    private LinearLayout buttons;
    private List<Related> related;
    private RecyclerView recyclerRelated;

    public LocalDetailsFragment() {
        // Required empty public constructor
    }

    public static LocalDetailsFragment newInstance(Bundle args) {
        LocalDetailsFragment m = new LocalDetailsFragment();
        m.setArguments(args);
        return m;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        this.activity = (AppCompatActivity) context;
    }

    @Override
    public void onStart() {
        super.onStart();
        ApiTask task = new ApiTask();
        task.setListener(this);
        Bundle args = getArguments();
        int id = -1;
        if (args != null) {
            id = args.getInt(KEY_ID, -1);
        }
        task.execute("GET", "/local/" + id);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =
                inflater.inflate(R.layout.fragment_local_details, container, false);
        setHasOptionsMenu(true);

        FloatingActionButton fab = activity.findViewById(R.id.fab);
        fab.setOnClickListener(v -> likeBehavior());

        AppBarLayout appBar = activity.findViewById(R.id.toolbar_collapse);
        appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {

            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    isShow = true;
                    MenuUtils.showOption(menu, R.id.menu_like);
                } else if (isShow) {
                    isShow = false;
                    MenuUtils.hideOption(menu, R.id.menu_like);
                }
            }
        });

        buttons = view.findViewById(R.id.focus);
        pb = view.findViewById(R.id.pb);
        recyclerRelated = view.findViewById(R.id.recycler_related);

        view.findViewById(R.id.btn_info).setOnClickListener(v ->
                loadFragment(InfoFragment.newInstance(argsInfo)));

        view.findViewById(R.id.btn_map).setOnClickListener(v -> {
            view.findViewById(R.id.frame_map).setVisibility(View.VISIBLE);
            loadFragment(MapFragment.newInstance(argsMap));
        });

        view.findViewById(R.id.btn_photos).setOnClickListener(v ->
                loadFragment(PhotoFragment.newInstance(argsPhotos)));

        view.findViewById(R.id.btn_contacts).setOnClickListener(v -> {
            loadFragment(ContactFragment.newInstance(argsContacts));
        });

        List<Related> eatList = new ArrayList<>();
        eatList.add(new Related(
                "nn",
                2,
                "Pizzaria BelBel",
                "https://media-cdn.tripadvisor.com/media/photo-s/0e/63/64/9c/photo0jpg.jpg"));
        eatList.add(new Related(
                "nn",
                2,
                "Restaurante Hede",
                "https://media-cdn.tripadvisor.com/media/photo-s/13/a0/09/36/le-hede-sur-l" +
                        "a-placette.jpg"));

        RelatedAdapter eatAdapter = new RelatedAdapter(eatList, this);
        RecyclerView recyclerEat = view.findViewById(R.id.recycler_eat);
        recyclerEat.setAdapter(eatAdapter);
        recyclerEat.setLayoutManager(new LinearLayoutManager(
                context, LinearLayoutManager.HORIZONTAL, false));

        List<Related> sleepList = new ArrayList<>();
        sleepList.add(new Related(
                "nn",
                2,
                "Hotel Horus",
                "https://r-cf.bstatic.com/images/hotel/max1024x768/345/34542046.jpg"));

        RelatedAdapter sleepAdapter = new RelatedAdapter(sleepList, this);
        RecyclerView recyclerSleep = view.findViewById(R.id.recycler_sleep);
        recyclerSleep.setAdapter(sleepAdapter);
        recyclerSleep.setLayoutManager(new LinearLayoutManager(
                context, LinearLayoutManager.HORIZONTAL, false));

        return view;
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        int id = R.id.frame_children_container;
        if (fragment instanceof MapFragment) {
            id = R.id.frame_map;
            swapViews(true);
        } else {
            swapViews(false);
        }
        transaction.replace(id, fragment);
        transaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.commit();
    }

    private void likeBehavior() {
        Toast.makeText(activity, "like", Toast.LENGTH_SHORT).show();
    }

    private void swapViews(boolean isMap) {
        if (getView() != null) {
            if (isMap) {
                getView().findViewById(R.id.frame_map).setVisibility(View.VISIBLE);
                getView().findViewById(R.id.frame_children_container).setVisibility(View.GONE);
            } else {
                getView().findViewById(R.id.frame_children_container).setVisibility(View.VISIBLE);
                getView().findViewById(R.id.frame_map).setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        this.menu = menu;
        inflater.inflate(R.menu.menu_fav_share, menu);
        MenuItem likeItem = menu.findItem(R.id.menu_like);
        MenuItem shareItem = menu.findItem(R.id.menu_share);
        likeItem.setIcon(MenuUtils.setIconColor(activity, likeItem));
        shareItem.setIcon(MenuUtils.setIconColor(activity, shareItem));
        MenuUtils.hideOption(menu, R.id.menu_like);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_share:
                Toast.makeText(activity, "share", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.menu_like:
                likeBehavior();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCardClick(View v, int i, String type) {
        Related item = related.get(i);
        if (type.equals(Related.KEY_PERSONALITY)) {
            Intent intent = new Intent(context, DetailsActivity.class);
            intent.putExtra(DetailsActivity.KEY_TYPE, DetailsActivity.KEY_PERSONALITY_DETAILS);
            intent.putExtra(DetailsActivity.KEY_ID, item.getId());
            intent.putExtra(DetailsActivity.KEY_TITLE, item.getTitle());
            intent.putExtra(DetailsActivity.KEY_IMAGE_PERSONALITY, item.getImage());
            startActivity(intent);
        } else if (type.equals(Related.KEY_LOCAL)) {
            Intent intent = new Intent(context, DetailsActivity.class);
            intent.putExtra(DetailsActivity.KEY_TYPE, DetailsActivity.KEY_HERITAGE_DETAILS);
            intent.putExtra(DetailsActivity.KEY_ID, item.getId());
            intent.putExtra(DetailsActivity.KEY_TITLE, item.getTitle());
            intent.putExtra(DetailsActivity.KEY_IMAGE, item.getImage());
            startActivity(intent);
        } else {
            Toast.makeText(activity, "Card", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onLikeClick(View v, int pos, String type) {
        Toast.makeText(activity, "Like", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onShareClick(View v, int pos, String type) {
        Toast.makeText(activity, "Share", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPre() {
        pb.setVisibility(View.VISIBLE);
        buttons.setVisibility(View.GONE);
    }

    @Override
    public void onPost(JSONObject res) {
        try {
            if (res.getInt("status_code") != 200) {
                String message = res.getString("message");
                ErrorDialog errorDialog = ErrorDialog.newInstance(message);
                errorDialog.show(getChildFragmentManager(), "error personalities list");
            } else {
                Local l = Local.getFullFromJson(res.getJSONObject("local"));

                argsInfo = new Bundle();
                argsInfo.putString(InfoFragment.KEY_INFO, l.getDesc());

                argsMap = new Bundle();
                argsMap.putBoolean(MapFragment.KEY_STATIC, true);
                argsMap.putDouble(MapFragment.KEY_LAT, l.getLatLng().latitude);
                argsMap.putDouble(MapFragment.KEY_LNG, l.getLatLng().longitude);

                Contact c = l.getContact();
                argsContacts = new Bundle();
                argsContacts.putString(ContactFragment.KEY_PHONE, c.getPhone());
                argsContacts.putString(ContactFragment.KEY_EMAIL, c.getEmail());
                argsContacts.putString(ContactFragment.KEY_WEBSITE, c.getWebsite());
                argsContacts.putString(ContactFragment.KEY_LOCATION, c.getAddress());

                argsPhotos = new Bundle();
                argsPhotos.putString(PhotoFragment.KEY_TYPE, PhotoFragment.KEY_TYPE);
                argsPhotos.putStringArrayList(PhotoFragment.KEY_PHOTOS,
                        new ArrayList<>(l.getImages()));

                related = l.getRelated();

                recyclerRelated.setAdapter(new RelatedAdapter(related, this));
                recyclerRelated.setLayoutManager(new LinearLayoutManager(
                        context, LinearLayoutManager.HORIZONTAL, false));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ErrorDialog errorDialog = new ErrorDialog();
            errorDialog.show(getChildFragmentManager(), "error personalities list");
        }
        pb.setVisibility(View.GONE);
        buttons.setVisibility(View.VISIBLE);
    }
}
