package pt.cmfelgueiras.app_patrimonio.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ir.apend.slider.model.Slide;
import ir.apend.slider.ui.Slider;
import pt.cmfelgueiras.app_patrimonio.R;
import pt.cmfelgueiras.app_patrimonio.activities.DetailsActivity;
import pt.cmfelgueiras.app_patrimonio.adapters.RelatedAdapter;
import pt.cmfelgueiras.app_patrimonio.classes.Personality;
import pt.cmfelgueiras.app_patrimonio.classes.Related;
import pt.cmfelgueiras.app_patrimonio.dialogs.ErrorDialog;
import pt.cmfelgueiras.app_patrimonio.tasks.ApiTask;
import pt.cmfelgueiras.app_patrimonio.utils.LoadImagesUtil;

import static pt.cmfelgueiras.app_patrimonio.tasks.ApiTask.IMG_TH_URL;

/**
 * A simple {@link Fragment} subclass.
 */
public class PersonalityDetailsFragment extends Fragment implements RelatedAdapter.RelatedListener, ApiTask.ApiListener {

    public static final String KEY_PERSONALTY_ID = "PERSONALITY_ID";
    public static final String KEY_TITLE = "PERSONALITY_TITLE";
    private AppCompatActivity activity;
    private Context context;
    private List<Related> list;
    private String title;
    private Slider slider;
    private NestedScrollView nestedScrollView;
    private RecyclerView recyclerRelated;
    private ProgressBar pb;

    public PersonalityDetailsFragment() {
        // Required empty public constructor
    }

    public static PersonalityDetailsFragment newInstance(Bundle args) {
        PersonalityDetailsFragment fragment = new PersonalityDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        this.activity = (AppCompatActivity) context;
    }

    @Override
    public void onStart() {
        super.onStart();
        ApiTask api = new ApiTask();
        api.setListener(this);
        Bundle args = getArguments();
        if (args != null) {
            int id = args.getInt(KEY_PERSONALTY_ID);
            title = args.getString(KEY_TITLE);
            String url = "/personality/" + id;
            api.execute("GET", url);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =
                inflater.inflate(R.layout.fragment_personality_details, container, false);

        nestedScrollView = view.findViewById(R.id.nested_scroll);
        nestedScrollView.setVisibility(View.GONE);
        pb = view.findViewById(R.id.pb);

        activity.findViewById(R.id.fab).setVisibility(View.GONE);
        activity.findViewById(R.id.toolbar_background_images).setVisibility(View.VISIBLE);

        CollapsingToolbarLayout collapsing = activity.findViewById(R.id.collapsing_layout);
        collapsing.setTitle(title);
        collapsing.setTitleEnabled(false);

        slider = activity.findViewById(R.id.toolbar_background_images);

        recyclerRelated = view.findViewById(R.id.recycler_related);

        List<Related> eatList = new ArrayList<>();
        eatList.add(new Related(
                "nn",
                1,
                "Pizzaria BelBel",
                "https://media-cdn.tripadvisor.com/media/photo-s/0e/63/64/9c/photo0jpg.jpg"));
        eatList.add(new Related(
                "nn",
                2,
                "Restaurante Hede",
                "https://media-cdn.tripadvisor.com/media/photo-s/13/a0/09/36/le-hede-sur-l" +
                        "a-placette.jpg"));

        RelatedAdapter eatAdapter = new RelatedAdapter(eatList, this);
        RecyclerView recyclerEat = view.findViewById(R.id.recycler_eat);
        recyclerEat.setAdapter(eatAdapter);
        recyclerEat.setLayoutManager(new LinearLayoutManager(
                context, LinearLayoutManager.HORIZONTAL, false));

        List<Related> sleepList = new ArrayList<>();
        sleepList.add(new Related(
                "nn",
                3,
                "Hotel Horus",
                "https://r-cf.bstatic.com/images/hotel/max1024x768/345/34542046.jpg"));

        RelatedAdapter sleepAdapter =
                new RelatedAdapter(sleepList, this);
        RecyclerView recyclerSleep = view.findViewById(R.id.recycler_sleep);
        recyclerSleep.setAdapter(sleepAdapter);
        recyclerSleep.setLayoutManager(new LinearLayoutManager(
                context, LinearLayoutManager.HORIZONTAL, false));
        return view;
    }

    @Override
    public void onCardClick(View v, int i, String type) {
        Related item = list.get(i);
        if (type.equals(Related.KEY_PERSONALITY)) {
            Intent intent = new Intent(context, DetailsActivity.class);
            intent.putExtra(DetailsActivity.KEY_TYPE, DetailsActivity.KEY_PERSONALITY_DETAILS);
            intent.putExtra(DetailsActivity.KEY_ID, item.getId());
            intent.putExtra(DetailsActivity.KEY_TITLE, item.getTitle());
            intent.putExtra(DetailsActivity.KEY_IMAGE_PERSONALITY, item.getImage());
            startActivity(intent);
        } else if (type.equals(Related.KEY_LOCAL)) {
            Intent intent = new Intent(context, DetailsActivity.class);
            intent.putExtra(DetailsActivity.KEY_TYPE, DetailsActivity.KEY_HERITAGE_DETAILS);
            intent.putExtra(DetailsActivity.KEY_ID, item.getId());
            intent.putExtra(DetailsActivity.KEY_TITLE, item.getTitle());
            intent.putExtra(DetailsActivity.KEY_IMAGE, item.getImage());
            startActivity(intent);
        } else {
            Toast.makeText(activity, "Card", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onLikeClick(View v, int pos, String type) {
        Toast.makeText(activity, "Like", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onShareClick(View v, int pos, String type) {
        Toast.makeText(activity, "Share", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPre() {
        pb.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPost(JSONObject res) {
        try {
            if (res.getInt("status_code") != 200) {
                String message = res.getString("message");
                ErrorDialog errorDialog = ErrorDialog.newInstance(message);
                errorDialog.show(getChildFragmentManager(), "error personalities list");
            } else {
                Personality p = Personality.getFullFromJson(res.getJSONObject("personality"));
                List<String> images = p.getImages();
                List<Slide> sliderList = new ArrayList<>();
                int numberPages = images.size();
                for (int i = 0; i < numberPages; i++) {
                    String url = IMG_TH_URL + images.get(i);
                    sliderList.add(new Slide(i, url, 0));
                }
                slider.addSlides(sliderList);

                ImageView profile = activity.findViewById(R.id.toolbar_image);
                LoadImagesUtil.loadImage(p.getImage(), profile);

                TextView date = activity.findViewById(R.id.date);
                date.setText(p.getDate());
                TextView job = activity.findViewById(R.id.job);
                job.setText(p.getJob());

                Bundle args = new Bundle();
                args.putString(InfoFragment.KEY_INFO, p.getDesc());
                FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_container, InfoFragment.newInstance(args));
                transaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                transaction.commit();

                list = p.getRelated();

                recyclerRelated.setAdapter(new RelatedAdapter(list, this));
                recyclerRelated.setLayoutManager(new LinearLayoutManager(
                        context, LinearLayoutManager.HORIZONTAL, false));

            }
        } catch (JSONException e) {
            e.printStackTrace();
            ErrorDialog errorDialog = new ErrorDialog();
            errorDialog.show(getChildFragmentManager(), "error personalities list");
        }

        pb.setVisibility(View.GONE);
        nestedScrollView.setVisibility(View.VISIBLE);
    }
}
