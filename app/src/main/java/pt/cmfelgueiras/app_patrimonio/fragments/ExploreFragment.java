package pt.cmfelgueiras.app_patrimonio.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pt.cmfelgueiras.app_patrimonio.R;
import pt.cmfelgueiras.app_patrimonio.activities.HeritageActivity;
import pt.cmfelgueiras.app_patrimonio.adapters.RelatedAdapter;
import pt.cmfelgueiras.app_patrimonio.classes.Related;
import pt.cmfelgueiras.app_patrimonio.utils.MenuUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExploreFragment extends Fragment implements RelatedAdapter.RelatedListener {

    private AppCompatActivity activity;
    private Menu menu;
    private Context context;
    private List<Related> list;

    public ExploreFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (AppCompatActivity) getActivity();
        this.context = context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_explore, container, false);
        setHasOptionsMenu(true);

        AppBarLayout appBar = activity.findViewById(R.id.toolbar_collapse);
        appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {

            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    isShow = true;
                    MenuUtils.showOption(menu, R.id.menu_agenda);
                } else if (isShow) {
                    isShow = false;
                    MenuUtils.hideOption(menu, R.id.menu_agenda);
                }
            }
        });

        setListeners(v);
        RecyclerView recyclerView = v.findViewById(R.id.recycler_view);
        list = new ArrayList<>();
        list.add(new Related("hh", 2, "Casa do Curral",
                "http://www.expressofelgueiras.com/wp-content/uploads/2017/03/ESTG-aspeto-" +
                        "geral.jpg"));
        recyclerView.setAdapter(
                new RelatedAdapter(list, this));
        recyclerView.setLayoutManager(new
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        return v;
    }

    private void setListeners(View view) {
        ImageView icon = activity.findViewById(R.id.toolbar_icon);
        icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_explore_24dp));

        activity.findViewById(R.id.fab)
                .setOnClickListener(v -> agendaBehavior());
        view.findViewById(R.id.btn_historical_heritage)
                .setOnClickListener(v -> start(HeritageActivity.KEY_HISTORICAL_HERITAGE));
        view.findViewById(R.id.btn_nature)
                .setOnClickListener(v -> start(HeritageActivity.KEY_NATURE));
        view.findViewById(R.id.btn_personality)
                .setOnClickListener(v -> start(HeritageActivity.KEY_PERSONALITY));
        view.findViewById(R.id.btn_restaurant)
                .setOnClickListener(v -> start(HeritageActivity.KEY_RESTAURANT));
        view.findViewById(R.id.btn_lodging)
                .setOnClickListener(v -> start(HeritageActivity.KEY_LODGING));
        view.findViewById(R.id.btn_timeline)
                .setOnClickListener(v -> start(HeritageActivity.KEY_TIMELINE));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        this.menu = menu;
        inflater.inflate(R.menu.menu_agenda, menu);
        MenuItem shareItem = menu.findItem(R.id.menu_agenda);
        shareItem.setIcon(MenuUtils.setIconColor(activity, shareItem));
        MenuUtils.hideOption(menu, R.id.menu_agenda);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_agenda) {
            agendaBehavior();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void agendaBehavior() {
        start(HeritageActivity.KEY_AGENDA);
    }

    private void start(String type) {
        Intent intent = new Intent(context, HeritageActivity.class);
        intent.putExtra(HeritageActivity.KEY_TYPE, type);
        startActivity(intent);
    }

    @Override
    public void onCardClick(View v, int i, String type) {
        /*Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra(DetailsActivity.KEY_TYPE, DetailsActivity.KEY_HISTORICAL_HERITAGE_DETAILS);
        intent.putExtra(DetailsActivity.KEY_IMAGE, list.get(i).getImage());
        intent.putExtra(DetailsActivity.KEY_TITLE, list.get(i).getTitle());
        startActivity(intent);*/
    }

    @Override
    public void onLikeClick(View v, int pos, String type) {
        Toast.makeText(activity, "Like", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onShareClick(View v, int pos, String type) {
        String text = "Look At This";
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        startActivity(Intent.createChooser(shareIntent, getString(R.string.intent_share)));
    }
}
