package pt.cmfelgueiras.app_patrimonio.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pt.cmfelgueiras.app_patrimonio.R;
import pt.cmfelgueiras.app_patrimonio.adapters.RelatedAdapter;
import pt.cmfelgueiras.app_patrimonio.classes.Related;

/**
 * A simple {@link Fragment} subclass.
 */
public class PathFragment extends Fragment implements RelatedAdapter.RelatedListener {

    public AppCompatActivity activity;
    public Context context;

    public PathFragment() {
        // Required empty public constructor
    }

    public static PathFragment newInstance(Bundle args) {
        PathFragment fragment = new PathFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        this.activity = (AppCompatActivity) getActivity();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_path, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        List<Related> list = new ArrayList<>();
        list.add(new Related(
                "nn",
                2,
                "Quinta da Maderne",
                "https://www.quintademaderne.com/wp-content/uploads/2016/06/quinta-photo_0" +
                        "01-1.jpg"));
        list.add(new Related(
                "nn",
                2,
                "Quinta da Lixa",
                "https://media-cdn.tripadvisor.com/media/photo-s/0d/42/a0/2b/getlstd-prope" +
                        "rty-photo.jpg"));
        list.add(new Related(
                "nn",
                2,
                "Quinta da Palmirinha",
                "https://scontent.flis7-1.fna.fbcdn.net/v/t1.0-0/q88/c55.2.390.390a/p526x3" +
                        "95/988899_665240480248672_6681097936175089736_n.jpg?_nc_cat=106&_nc_ht=s" +
                        "content.flis7-1.fna&oh=658e01aaf06b022823c92741a1bc6800&oe=5D5CA014"));

        recyclerView.setAdapter(new RelatedAdapter(list, this));
        recyclerView.setLayoutManager(new
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        return view;
    }

    @Override
    public void onCardClick(View v, int i, String type) {
        Toast.makeText(activity, "card", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLikeClick(View v, int pos, String type) {
        Toast.makeText(activity, "Like", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onShareClick(View v, int pos, String type) {
        Toast.makeText(activity, "Share", Toast.LENGTH_SHORT).show();
    }
}
