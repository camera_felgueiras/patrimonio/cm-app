package pt.cmfelgueiras.app_patrimonio.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import pt.cmfelgueiras.app_patrimonio.R;
import pt.cmfelgueiras.app_patrimonio.activities.DetailsActivity;
import pt.cmfelgueiras.app_patrimonio.adapters.CategoriesAdapter;
import pt.cmfelgueiras.app_patrimonio.adapters.RecyclerAdapter;
import pt.cmfelgueiras.app_patrimonio.classes.Category;
import pt.cmfelgueiras.app_patrimonio.classes.Local;
import pt.cmfelgueiras.app_patrimonio.dialogs.ErrorDialog;
import pt.cmfelgueiras.app_patrimonio.tasks.ApiTask;

/**
 * A simple {@link Fragment} subclass.
 */
public class LocalFragment extends Fragment implements RecyclerAdapter.ListListener, ApiTask.ApiListener {

    public static final String KEY_HISTORICAL_HERITAGE = "HISTORICAL_HERITAGE";
    public static final String KEY_NATURE = "NATURE";
    private static final String KEY_TYPE = "TYPE";
    public AppCompatActivity activity;
    public Context context;

    private RecyclerView recyclerView, recyclerCategories;
    private ProgressBar pb;
    private List<Local> list;
    private List<Category> categoriesList;

    public LocalFragment() {
        // Required empty public constructor
    }

    public static LocalFragment newInstance(String type) {

        Bundle args = new Bundle();
        args.putString(KEY_TYPE, type);

        LocalFragment fragment = new LocalFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        this.activity = (AppCompatActivity) getActivity();
    }

    @Override
    public void onStart() {
        super.onStart();
        ApiTask taskList = new ApiTask();
        ApiTask taskCategories = new ApiTask();
        taskList.setListener(this);
        taskCategories.setListener(this);
        Bundle args = getArguments();
        if (args != null) {
            String urlList = "";
            String urlCategories;
            String type = args.getString(KEY_TYPE);
            if (type != null && type.equals(KEY_HISTORICAL_HERITAGE)) {
                try {
                    String query = "Património Histórico";
                    urlList = "/local?type=" + URLEncoder.encode(query, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                urlCategories = "/category?type=1";
            } else {
                urlList = "/local?type=Natureza";
                urlCategories = "/category?type=2";
            }
            taskList.execute("GET", urlList);
            taskCategories.execute("GET", urlCategories);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerCategories = view.findViewById(R.id.recycler_categories);
        pb = view.findViewById(R.id.pb);
        return view;
    }

    @Override
    public void onCardClick(View v, int i, String type) {
        Local item = list.get(i);
        Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra(DetailsActivity.KEY_TYPE, DetailsActivity.KEY_HERITAGE_DETAILS);
        intent.putExtra(DetailsActivity.KEY_ID, item.getId());
        intent.putExtra(DetailsActivity.KEY_TITLE, item.getTitle());
        intent.putExtra(DetailsActivity.KEY_IMAGE, item.getMainImage());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onPre() {
        pb.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPost(JSONObject res) {
        try {
            Log.d("log", "onPost: " + res);
            if (res.getInt("status_code") != 200) {
                String message = res.getString("message");
                ErrorDialog errorDialog = ErrorDialog.newInstance(message);
                errorDialog.show(getChildFragmentManager(), "error local list");
            } else {
                if (res.has("locals")) {
                    handleLocals(res);
                } else if (res.has("categories")) {
                    handleCategories(res);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ErrorDialog errorDialog = new ErrorDialog();
            errorDialog.show(getChildFragmentManager(), "error personalities list");
        }
        pb.setVisibility(View.GONE);
    }

    private void handleLocals(JSONObject res) throws JSONException {
        list = new ArrayList<>();
        JSONArray locals = res.getJSONArray("locals");
        int localsLength = locals.length();
        for (int i = 0; i < localsLength; i++) {
            JSONObject obj = locals.getJSONObject(i);
            list.add(Local.getFromJson(obj));
        }

        recyclerView.setAdapter(new RecyclerAdapter(
                list, RecyclerAdapter.TYPE_LIST_SIMPLE, this));
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
    }

    private void handleCategories(JSONObject res) throws JSONException {
        categoriesList = new ArrayList<>();

        JSONArray categories = res.getJSONArray("categories");
        int categoriesSize = categories.length();
        for (int i = 0; i < categoriesSize; i++) {
            JSONObject obj = categories.getJSONObject(i);
            categoriesList.add(Category.getFromJson(obj));
        }
        recyclerCategories.setVisibility(View.VISIBLE);
        recyclerCategories.setAdapter(new CategoriesAdapter(categoriesList));
        recyclerCategories.setLayoutManager(new LinearLayoutManager(
                context, LinearLayoutManager.HORIZONTAL, false));
    }
}
