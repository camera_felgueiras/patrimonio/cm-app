package pt.cmfelgueiras.app_patrimonio.fragments;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Objects;

import pt.cmfelgueiras.app_patrimonio.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactFragment extends Fragment {

    public static final String KEY_PHONE = "PHONE";
    public static final String KEY_EMAIL = "EMAIL";
    public static final String KEY_WEBSITE = "WEBSITE";
    public static final String KEY_LOCATION = "LOCATION";

    public AppCompatActivity activity;
    public Context context;

    public ContactFragment() {
        // Required empty public constructor
    }

    public static ContactFragment newInstance(Bundle args) {
        ContactFragment fragment = new ContactFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        this.activity = (AppCompatActivity) getActivity();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact, container, false);

        TextView phone, email, website, location;
        String phoneText, emailText, websiteText, locationText;

        phone = view.findViewById(R.id.phone);
        email = view.findViewById(R.id.email);
        website = view.findViewById(R.id.website);
        location = view.findViewById(R.id.location);

        Bundle args = getArguments();
        if (args != null) {
            phoneText = args.getString(KEY_PHONE);
            if (phoneText != null && phoneText.equals("null")) {
                phoneText = getString(R.string.no_phone);
            }
            emailText = args.getString(KEY_EMAIL);
            if (emailText != null && emailText.equals("null")) {
                emailText = getString(R.string.no_email);
            }
            websiteText = args.getString(KEY_WEBSITE);
            if (websiteText != null && websiteText.equals("null")) {
                websiteText = getString(R.string.no_website);
            }
            locationText = args.getString(KEY_LOCATION);
        } else {
            phoneText = getString(R.string.error);
            emailText = getString(R.string.error);
            websiteText = getString(R.string.error);
            locationText = getString(R.string.error);
        }

        if (phoneText != null && !phoneText.equals(getString(R.string.no_phone))) {
            phone.setOnClickListener(v -> {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + phone.getText().toString()));
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    searchMarket("Phone");
                }
            });
        }
        if (emailText != null && !emailText.equals(getString(R.string.no_email))) {
            email.setOnClickListener(v -> {
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:" + email.getText().toString()));
                try {
                    startActivity(Intent.createChooser(intent, getString(R.string.mail_with)));
                } catch (ActivityNotFoundException e) {
                    searchMarket("Email");
                }
            });
        }
        if (websiteText != null && websiteText.equals(getString(R.string.no_website))) {
            website.setOnClickListener(v -> {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(website.getText().toString()));
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    searchMarket("Browser");
                }
            });
        }
        location.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("geo:0,0?q=" + location.getText().toString()));
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                searchMarket("Maps");
            }
        });

        phone.setText(phoneText);
        email.setText(emailText);
        website.setText(websiteText);
        location.setText(locationText);
        return view;
    }

    private void searchMarket(String query) {
        String q = "market://search?q=" + query;
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(q));
        startActivity(i);
    }
}
