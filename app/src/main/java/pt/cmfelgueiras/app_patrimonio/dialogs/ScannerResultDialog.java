package pt.cmfelgueiras.app_patrimonio.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import pt.cmfelgueiras.app_patrimonio.R;
import pt.cmfelgueiras.app_patrimonio.activities.DetailsActivity;
import pt.cmfelgueiras.app_patrimonio.tasks.ApiTask;
import pt.cmfelgueiras.app_patrimonio.utils.LoadImagesUtil;

public class ScannerResultDialog extends DialogFragment {

    private static final String KEY_SCANNER_RESULT = "SCANNER_RESULT";

    private AppCompatActivity activity;
    private Listener listener;

    public static ScannerResultDialog newInstance(String result) {
        ScannerResultDialog y = new ScannerResultDialog();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_SCANNER_RESULT, result);
        y.setArguments(bundle);
        return y;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (AppCompatActivity) getActivity();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.dialog_scanner_result, null);

        ImageView image = dialogView.findViewById(R.id.image);
        TextView text = dialogView.findViewById(R.id.title);

        String imageUrl = "";
        String title = "";
        int id = -1;
        String type = "";
        Bundle args = getArguments();
        if (args != null) {
            try {
                JSONObject json = new JSONObject(args.getString(KEY_SCANNER_RESULT));
                Log.d("log", "onCreateDialog: " + json.toString());
                imageUrl = ApiTask.IMG_TH_URL + json.getString("image");
                title = json.getString("title");
                type = json.getString("type");
                id = json.getInt("id");

                LoadImagesUtil.loadImage(imageUrl, image);
                text.setText(title);
            } catch (JSONException e) {
                e.printStackTrace();
                text.setText(R.string.invalid_qr);
            }
        }

        String finalImageUrl = imageUrl;
        String finalTitle = title;
        String finalType = type;
        int finalId = id;
        boolean isFormatted = true;
        if (title.equals("") || type.equals("") || imageUrl.equals("") || id == -1) {
            isFormatted = false;
        }
        if (isFormatted) {
            builder.setView(dialogView)
                    .setPositiveButton(getString(R.string.dialog_ok), (dialog, which) -> {
                        String detailsType;
                        if (finalType.equals("heritage")) {
                            detailsType = DetailsActivity.KEY_HERITAGE_DETAILS;
                        } else if (finalType.equals("personality")) {
                            detailsType = DetailsActivity.KEY_PERSONALITY_DETAILS;
                        } else {
                            detailsType = DetailsActivity.KEY_ROUTES_DETAILS;
                        }
                        listener.onPositiveClick(finalImageUrl, finalTitle, finalId, detailsType);
                    })
                    .setNegativeButton(getString(R.string.dialog_cancel), (dialog, which) ->
                            listener.onNegativeClick(this.getDialog()))
                    .setOnDismissListener(dialog -> listener.onDismiss(dialog));
        } else {
            image.setVisibility(View.GONE);
            builder.setView(dialogView)
                    .setNegativeButton(R.string.dialog_ok, (dialog, which) ->
                            listener.onNegativeClick(this.getDialog()));
        }
            return builder.create();
    }

    public interface Listener {
        void onPositiveClick(String imageUrl, String title, int id, String type);

        void onNegativeClick(Dialog dialog);

        void onDismiss(DialogInterface dialog);
    }
}
