package pt.cmfelgueiras.app_patrimonio.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;

import pt.cmfelgueiras.app_patrimonio.R;

public class YearPickerDialog extends DialogFragment {

    private static final String KEY_CURRENT_SELECTED = "CURRENT_SELECTED_YEAR";
    private static final String KEY_MIN_VALUE = "MIN_VALUE";
    private static final String KEY_MAX_VALUE = "MAX_VALUE";

    private AppCompatActivity activity;
    private Listener listener;

    public static YearPickerDialog newInstance(int currentYear, int minValue, int maxValue) {
        YearPickerDialog y = new YearPickerDialog();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_CURRENT_SELECTED, currentYear);
        bundle.putInt(KEY_MIN_VALUE, minValue);
        bundle.putInt(KEY_MAX_VALUE, maxValue);
        y.setArguments(bundle);
        return y;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (AppCompatActivity) getActivity();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.dialog_year_picker, null);

        final NumberPicker yearPicker = dialogView.findViewById(R.id.year_picker);
        yearPicker.setOnValueChangedListener((picker, oldVal, newVal) ->
                picker.setValue((newVal < oldVal) ? oldVal - 5 : oldVal + 5));
        Bundle args = getArguments();
        if (args != null) {
            yearPicker.setMaxValue(args.getInt(KEY_MAX_VALUE));
            yearPicker.setMinValue(args.getInt(KEY_MIN_VALUE));
            yearPicker.setValue(args.getInt(KEY_CURRENT_SELECTED));
        } else {
            yearPicker.setValue(2000);
        }

        builder.setView(dialogView)
                .setPositiveButton("OK", (dialog, which) ->
                        listener.onYearSet(yearPicker.getValue()))
                .setNegativeButton("CANCEL", (dialog, which) ->
                        YearPickerDialog.this.getDialog().dismiss());

        return builder.create();
    }

    public interface Listener {
        void onYearSet(int year);
    }
}
