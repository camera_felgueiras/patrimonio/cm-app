package pt.cmfelgueiras.app_patrimonio.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;

import pt.cmfelgueiras.app_patrimonio.R;
import pt.cmfelgueiras.app_patrimonio.activities.MainActivity;

public class ErrorDialog extends DialogFragment {

    private static final String KEY_TITLE = "TITLE";
    private static final String KEY_MESSAGE = "MESSAGE";
    private AppCompatActivity activity;

    public static ErrorDialog newInstance(String message) {
        Bundle args = new Bundle();
        args.putString(KEY_MESSAGE, message);
        ErrorDialog fragment = new ErrorDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public static ErrorDialog newInstance(String title, String message) {
        Bundle args = new Bundle();
        args.putString(KEY_TITLE, title);
        args.putString(KEY_MESSAGE, message);
        ErrorDialog fragment = new ErrorDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (AppCompatActivity) getActivity();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        setCancelable(false);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        Bundle args = getArguments();
        String title, message;
        if (args != null) {
            title = args.getString(KEY_TITLE);
            message = args.getString(KEY_MESSAGE);
        } else {
            title = getString(R.string.something_went_wrong);
            message = getString(R.string.unknown);
        }
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(R.string.dialog_ok, (dialog, which) -> {
            dialog.dismiss();
            startActivity(new Intent(activity, MainActivity.class));
        });

        return builder.create();
    }
}
