package pt.cmfelgueiras.app_patrimonio.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;

import com.github.chrisbanes.photoview.PhotoView;

import pt.cmfelgueiras.app_patrimonio.R;
import pt.cmfelgueiras.app_patrimonio.utils.LoadImagesUtil;

import static pt.cmfelgueiras.app_patrimonio.tasks.ApiTask.IMG_URL;

public class ImageViewDialog extends DialogFragment {

    private static final String KEY_IMAGE_URL = "IMAGE_URL";

    private AppCompatActivity activity;

    public static ImageViewDialog newInstance(String img) {
        ImageViewDialog y = new ImageViewDialog();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_IMAGE_URL, img);
        y.setArguments(bundle);
        return y;
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (AppCompatActivity) getActivity();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.dialog_image_view, null);
        PhotoView photoView = dialogView.findViewById(R.id.image_view);

        Bundle args = getArguments();

        String url = "";
        if (args != null) {
            url = IMG_URL + args.getString(KEY_IMAGE_URL);
        }
        LoadImagesUtil.loadImageResize(url, photoView, 500, 500);

        builder.setView(dialogView);
        return builder.create();
    }
}
