package pt.cmfelgueiras.app_patrimonio.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;

import java.util.ArrayList;
import java.util.List;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import pt.cmfelgueiras.app_patrimonio.dialogs.ScannerResultDialog;
import pt.cmfelgueiras.app_patrimonio.utils.PermissionUtils;

public class ScannerActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler,
        ScannerResultDialog.Listener {

    private static final String FLASH_STATE = "FLASH_STATE";
    private static final String AUTO_FOCUS_STATE = "AUTO_FOCUS_STATE";
    private static final String CAMERA_ID = "CAMERA_ID";

    private ZXingScannerView scannerView;
    private boolean flash;
    private boolean autoFocus;
    private int cameraId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            flash = savedInstanceState.getBoolean(FLASH_STATE, false);
            autoFocus = savedInstanceState.getBoolean(AUTO_FOCUS_STATE, true);
            cameraId = savedInstanceState.getInt(CAMERA_ID, -1);
        } else {
            flash = false;
            autoFocus = true;
            cameraId = -1;
        }
        scannerView = new ZXingScannerView(this);
        scannerView.setAspectTolerance(0.5f);

        setFormats();
        setContentView(scannerView);

        int currentApiVersion = Build.VERSION.SDK_INT;
        if (currentApiVersion >= Build.VERSION_CODES.M) {
            String cam = android.Manifest.permission.CAMERA;
            if (!PermissionUtils.checkPermission(this, cam)) {
                PermissionUtils.requestPermission(this, cam);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    private void setFormats() {
        List<BarcodeFormat> list = new ArrayList<>();
        list.add(BarcodeFormat.QR_CODE);
        scannerView.setFormats(list);
    }

    @Override
    protected void onResume() {
        super.onResume();
        scannerView.setResultHandler(this);
        scannerView.setFlash(flash);
        scannerView.setAutoFocus(autoFocus);
        scannerView.startCamera(cameraId);
    }

    @Override
    public void onBackPressed() {
        Intent i = getIntent();
        if (i.getBooleanExtra("SHORT", false)) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra(MainActivity.KEY_TYPE, MainActivity.KEY_SCAN);
            startActivity(intent);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        scannerView.stopCamera();
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putBoolean(FLASH_STATE, flash);
        outState.putBoolean(AUTO_FOCUS_STATE, autoFocus);
        outState.putInt(CAMERA_ID, cameraId);
    }

    @Override
    public void handleResult(Result rawResult) {
        ScannerResultDialog dialog = ScannerResultDialog.newInstance(rawResult.getText());
        dialog.setListener(this);
        dialog.show(getSupportFragmentManager(), "result");
    }

    @Override
    public void onPositiveClick(String imageUrl, String title, int id, String type) {
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra(DetailsActivity.KEY_TYPE, type);
        intent.putExtra(DetailsActivity.KEY_IMAGE, imageUrl);
        intent.putExtra(DetailsActivity.KEY_TITLE, title);
        intent.putExtra(DetailsActivity.KEY_ID, id);
        startActivity(intent);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        scannerView.resumeCameraPreview(this);
    }

    @Override
    public void onNegativeClick(Dialog dialog) {
        scannerView.resumeCameraPreview(this);
        dialog.dismiss();
    }
}
