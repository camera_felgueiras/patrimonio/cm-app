package pt.cmfelgueiras.app_patrimonio.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import pt.cmfelgueiras.app_patrimonio.R;
import pt.cmfelgueiras.app_patrimonio.fragments.AgendaDetailsFragment;
import pt.cmfelgueiras.app_patrimonio.fragments.LocalDetailsFragment;
import pt.cmfelgueiras.app_patrimonio.fragments.MaterialHeritageDetailsFragment;
import pt.cmfelgueiras.app_patrimonio.fragments.PersonalityDetailsFragment;
import pt.cmfelgueiras.app_patrimonio.fragments.RouteDetailsFragment;
import pt.cmfelgueiras.app_patrimonio.utils.LoadImagesUtil;

public class DetailsActivity extends AppCompatActivity {

    public static final String KEY_TYPE = "TYPE";
    public static final String KEY_TITLE = "TITLE";
    public static final String KEY_ID = "ID";
    public static final String KEY_IMAGE = "IMAGE";
    public static final String KEY_IMAGE_PERSONALITY = "IMAGE_PERSONALITY";

    public static final String KEY_ROUTES_DETAILS = "ROUTES_DETAILS";
    public static final String KEY_AGENDA_DETAILS = "AGENDA_DETAILS";
    public static final String KEY_HERITAGE_DETAILS = "HERITAGE_DETAILS";
    public static final String KEY_HISTORICAL_HERITAGE_DETAILS = "HISTORICAL_HERITAGE_DETAILS";
    public static final String KEY_NATURE_DETAILS = "NATURE_DETAILS";
    public static final String KEY_PERSONALITY_DETAILS = "PERSONALITY_DETAILS";
    public static final String KEY_RESTAURANT_DETAILS = "RESTAURANT_DETAILS";
    public static final String KEY_LODGING_DETAILS = "LODGING_DETAILS";

    private boolean first;
    private BottomNavigationView.OnNavigationItemSelectedListener bottomNavListener = item -> {
        String type = null;
        switch (item.getItemId()) {
            case R.id.navigation_explore:
                type = MainActivity.KEY_EXPLORE;
                break;
            case R.id.navigation_routes:
                type = MainActivity.KEY_ROUTES;
                break;
            case R.id.navigation_ticketline:
                type = MainActivity.KEY_TICKETLINE;
                break;
            case R.id.navigation_scan:
                type = MainActivity.KEY_SCAN;
                break;
            case R.id.navigation_favorite:
                type = MainActivity.KEY_FAVORITE;
                break;
        }
        if (!first) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra(MainActivity.KEY_TYPE, type);
            startActivity(intent);
        } else {
            first = false;
        }
        return true;
    };

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        BottomNavigationView navigationView = findViewById(R.id.bottom_navigation);
        navigationView.setOnNavigationItemSelectedListener(bottomNavListener);

        String type = getIntent().getStringExtra(KEY_TYPE);
        String title = getIntent().getStringExtra(KEY_TITLE);
        String image = getIntent().getStringExtra(KEY_IMAGE);
        int id = getIntent().getIntExtra(KEY_ID, -1);

        switch (type) {
            case KEY_ROUTES_DETAILS:
                first = true;
                navigationView.setSelectedItemId(R.id.navigation_routes);
                loadFragment(new RouteDetailsFragment());
                break;
            case KEY_AGENDA_DETAILS:
                loadFragment(new AgendaDetailsFragment());
                break;
            case KEY_PERSONALITY_DETAILS:
                findViewById(R.id.personality_toolbar).setVisibility(View.VISIBLE);
                Bundle bundle = new Bundle();
                bundle.putInt(PersonalityDetailsFragment.KEY_PERSONALTY_ID, id);
                bundle.putString(PersonalityDetailsFragment.KEY_TITLE, title);
                loadFragment(PersonalityDetailsFragment.newInstance(bundle));
                break;
            case KEY_HERITAGE_DETAILS:
                Bundle local = new Bundle();
                local.putInt(LocalDetailsFragment.KEY_ID, id);
                loadFragment(LocalDetailsFragment.newInstance(local));
                break;
            default:
                Bundle args = new Bundle();
                args.putString("type", type);
                loadFragment(MaterialHeritageDetailsFragment.newInstance(args));
                break;
        }

        if (!type.equals(KEY_PERSONALITY_DETAILS)) {
            ImageView imageView = findViewById(R.id.toolbar_background_image);
            imageView.setVisibility(View.VISIBLE);
            LoadImagesUtil.loadImage(image, imageView);
        }

        Toolbar toolbar = findViewById(R.id.toolbar_c);
        setSupportActionBar(toolbar);

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setTitle(title);
            bar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
