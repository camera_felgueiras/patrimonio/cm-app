package pt.cmfelgueiras.app_patrimonio.activities;

import android.Manifest;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import pt.cmfelgueiras.app_patrimonio.R;
import pt.cmfelgueiras.app_patrimonio.fragments.ExploreFragment;
import pt.cmfelgueiras.app_patrimonio.fragments.FavoriteFragment;
import pt.cmfelgueiras.app_patrimonio.fragments.RouteFragment;
import pt.cmfelgueiras.app_patrimonio.fragments.ScanFragment;
import pt.cmfelgueiras.app_patrimonio.fragments.TicketLineFragment;
import pt.cmfelgueiras.app_patrimonio.utils.PermissionUtils;

public class MainActivity extends AppCompatActivity {

    public static final String KEY_TYPE = "TYPE";
    public static final String KEY_EXPLORE = "EXPLORE";
    public static final String KEY_ROUTES = "ROUTES";
    public static final String KEY_TICKETLINE = "TICKETLINE";
    public static final String KEY_SCAN = "SCAN";
    public static final String KEY_FAVORITE = "FAVORITE";

    private CollapsingToolbarLayout toolbarLayout;
    private BottomNavigationView.OnNavigationItemSelectedListener bottomNavListener = item -> {
        boolean isExplore = false;
        switch (item.getItemId()) {
            case R.id.navigation_explore:
                isExplore = true;
                loadFragment(new ExploreFragment());
                setToolbarTitle(R.string.explore);
                break;
            case R.id.navigation_routes:
                loadFragment(new RouteFragment());
                setToolbarTitle(R.string.route);
                break;
            case R.id.navigation_ticketline:
                loadFragment(new TicketLineFragment());
                setToolbarTitle(R.string.ticketline);
                break;
            case R.id.navigation_scan:
                loadFragment(new ScanFragment());
                setToolbarTitle(R.string.scan);
                break;
            case R.id.navigation_favorite:
                loadFragment(new FavoriteFragment());
                setToolbarTitle(R.string.favorite);
                break;
        }
        if (!isExplore) {
            findViewById(R.id.fab).setVisibility(View.GONE);
        } else {
            findViewById(R.id.fab).setVisibility(View.VISIBLE);
        }
        return true;
    };

    private void setToolbarTitle(int string) {
        toolbarLayout.setTitle(getString(string));
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar_c);
        setSupportActionBar(toolbar);

        BottomNavigationView navigationView = findViewById(R.id.bottom_navigation);
        navigationView.setOnNavigationItemSelectedListener(bottomNavListener);

        toolbarLayout = findViewById(R.id.collapsing_layout);

        String type = getIntent().getStringExtra(KEY_TYPE);

        int id;
        switch (type != null ? type : KEY_TYPE) {
            case KEY_EXPLORE:
                id = R.id.navigation_explore;
                break;
            case KEY_ROUTES:
                id = R.id.navigation_routes;
                break;
            case KEY_TICKETLINE:
                id = R.id.navigation_ticketline;
                break;
            case KEY_SCAN:
                id = R.id.navigation_scan;
                break;
            case KEY_FAVORITE:
                id = R.id.navigation_favorite;
                break;
            default:
                id = R.id.navigation_explore;
                break;
        }
        navigationView.setSelectedItemId(id);
    }

    @Override
    protected void onResume() {
        super.onResume();

        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};
        if (!PermissionUtils.checkPermission(getApplicationContext(), permissions[0])) {
            PermissionUtils.requestPermission(this, permissions);
            return;
        }
    }
}
