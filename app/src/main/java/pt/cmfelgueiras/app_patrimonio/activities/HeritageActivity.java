package pt.cmfelgueiras.app_patrimonio.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import pt.cmfelgueiras.app_patrimonio.R;
import pt.cmfelgueiras.app_patrimonio.fragments.AgendaFragment;
import pt.cmfelgueiras.app_patrimonio.fragments.LocalFragment;
import pt.cmfelgueiras.app_patrimonio.fragments.LodgingFragment;
import pt.cmfelgueiras.app_patrimonio.fragments.PersonalitiesFragment;
import pt.cmfelgueiras.app_patrimonio.fragments.RestaurantFragment;
import pt.cmfelgueiras.app_patrimonio.fragments.TimelineFragment;

public class HeritageActivity extends AppCompatActivity {

    public static final String KEY_TYPE = "TYPE";
    public static final String KEY_HISTORICAL_HERITAGE = "HISTORICAL_HERITAGE";
    public static final String KEY_NATURE = "NATURE";
    public static final String KEY_PERSONALITY = "PERSONALITY";
    public static final String KEY_AGENDA = "AGENDA";
    public static final String KEY_TIMELINE = "TIMELINE";
    public static final String KEY_RESTAURANT = "RESTAURANT";
    public static final String KEY_LODGING = "LODGING";

    private BottomNavigationView.OnNavigationItemSelectedListener bottomNavListener = item -> {
        String type = null;
        switch (item.getItemId()) {
            case R.id.navigation_explore:
                type = MainActivity.KEY_EXPLORE;
                break;
            case R.id.navigation_routes:
                type = MainActivity.KEY_ROUTES;
                break;
            case R.id.navigation_ticketline:
                type = MainActivity.KEY_TICKETLINE;
                break;
            case R.id.navigation_scan:
                type = MainActivity.KEY_SCAN;
                break;
            case R.id.navigation_favorite:
                type = MainActivity.KEY_FAVORITE;
                break;
        }
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(MainActivity.KEY_TYPE, type);
        startActivity(intent);
        return true;
    };

    private ActionBar bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_material_heritage);

        String type = getIntent().getStringExtra(KEY_TYPE);

        Toolbar toolbar = findViewById(R.id.toolbar_n);
        setSupportActionBar(toolbar);
        bar = getSupportActionBar();

        switch (type) {
            case KEY_HISTORICAL_HERITAGE:
                setToolbarTitle(R.string.menu_historical_heritage);
                loadFragment(LocalFragment.newInstance(LocalFragment.KEY_HISTORICAL_HERITAGE));
                break;
            case KEY_NATURE:
                setToolbarTitle(R.string.menu_nature);
                loadFragment(LocalFragment.newInstance(LocalFragment.KEY_NATURE));
                break;
            case KEY_PERSONALITY:
                setToolbarTitle(R.string.menu_personality);
                loadFragment(new PersonalitiesFragment());
                break;
            case KEY_AGENDA:
                setToolbarTitle(R.string.menu_agenda);
                loadFragment(new AgendaFragment());
                break;
            case KEY_TIMELINE:
                setToolbarTitle(R.string.menu_timeline);
                loadFragment(new TimelineFragment());
                break;
            case KEY_RESTAURANT:
                setToolbarTitle(R.string.menu_restaurants);
                loadFragment(new RestaurantFragment());
                break;
            case KEY_LODGING:
                setToolbarTitle(R.string.menu_lodging);
                loadFragment(new LodgingFragment());
                break;
        }

        BottomNavigationView navigationView = findViewById(R.id.bottom_navigation);
        navigationView.setOnNavigationItemSelectedListener(bottomNavListener);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent i = new Intent(this, MainActivity.class);
            i.putExtra(MainActivity.KEY_TYPE, MainActivity.KEY_EXPLORE);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(MainActivity.KEY_TYPE, MainActivity.KEY_EXPLORE);
        startActivity(intent);
    }

    private void setToolbarTitle(final int title) {
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
            bar.setTitle(title);
        }
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
