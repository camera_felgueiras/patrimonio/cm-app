package pt.cmfelgueiras.app_patrimonio.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.GoogleMap;

import pt.cmfelgueiras.app_patrimonio.R;
import pt.cmfelgueiras.app_patrimonio.fragments.MapFragment;

public class MapActivity extends AppCompatActivity {

    public static final String KEY_TYPE = "TYPE";
    public static final String KEY_LAT = "LAT";
    public static final String KEY_LNG = "LNG";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        int mapType = getIntent().getIntExtra(KEY_TYPE, GoogleMap.MAP_TYPE_NORMAL);
        double lat = getIntent().getDoubleExtra(KEY_LAT, 0);
        double lng = getIntent().getDoubleExtra(KEY_LNG, 0);

        Bundle bundle = new Bundle();
        bundle.putBoolean(MapFragment.KEY_STATIC, false);
        bundle.putInt(MapFragment.KEY_TYPE, mapType);
        bundle.putDouble(MapFragment.KEY_LAT, lat);
        bundle.putDouble(MapFragment.KEY_LNG, lng);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, MapFragment.newInstance(bundle));
        transaction.commit();
    }
}
