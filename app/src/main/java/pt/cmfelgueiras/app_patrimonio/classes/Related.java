package pt.cmfelgueiras.app_patrimonio.classes;

import org.json.JSONException;
import org.json.JSONObject;

import static pt.cmfelgueiras.app_patrimonio.tasks.ApiTask.IMG_TH_URL;

public class Related {
    public static final String KEY_PERSONALITY = "PERSONALITY";
    public static final String KEY_LOCAL = "LOCAL";

    private int id;
    private String type, title, image;

    public Related(String type, int id, String title, String image) {
        this.type = type;
        this.id = id;
        this.title = title;
        this.image = image;
    }

    static Related getFromJson(String key, JSONObject obj) throws JSONException {
        int id = obj.getInt("related_id");
        String name = obj.getString("related_name");
        String image = obj.getString("related_image");
        String url = IMG_TH_URL + image;
        return new Related(key, id, name, url);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
