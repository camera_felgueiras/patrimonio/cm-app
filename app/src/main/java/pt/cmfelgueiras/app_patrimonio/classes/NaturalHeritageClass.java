package pt.cmfelgueiras.app_patrimonio.classes;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public class NaturalHeritageClass {
    private String title, likes, mainImage, information;
    private LatLng coordinates;
    private List<String> images;

    public NaturalHeritageClass(String title,
                                String likes,
                                String mainImage,
                                String information,
                                LatLng coordinates) {
        this.title = title;
        this.likes = likes;
        this.mainImage = mainImage;
        this.information = information;
        this.coordinates = coordinates;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getMainImage() {
        return mainImage;
    }

    public void setMainImage(String mainImage) {
        this.mainImage = mainImage;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public LatLng getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(LatLng coordinates) {
        this.coordinates = coordinates;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }
}
