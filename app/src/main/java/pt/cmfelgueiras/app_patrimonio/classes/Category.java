package pt.cmfelgueiras.app_patrimonio.classes;

import org.json.JSONException;
import org.json.JSONObject;

public class Category {
    private int id, idType;
    private String name;

    private Category(int id, int idType, String name) {
        this.id = id;
        this.idType = idType;
        this.name = name;
    }

    public static Category getFromJson(JSONObject obj) throws JSONException {
        int id = obj.getInt("id");
        int idType = obj.getInt("id_type");
        String name = obj.getString("name");
        return new Category(id, idType, name);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdType() {
        return idType;
    }

    public void setIdType(int idType) {
        this.idType = idType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
