package pt.cmfelgueiras.app_patrimonio.classes;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static pt.cmfelgueiras.app_patrimonio.tasks.ApiTask.IMG_TH_URL;

public class Local {
    private int id, likes;
    private String title, desc, mainImage, type;
    private List<Category> categories;
    private LatLng latLng;
    private Contact contact;
    private List<String> images;
    private List<Related> related;

    public Local(int id, int likes, String title, String mainImage,
                 List<Category> categories) {

        this.id = id;
        this.likes = likes;
        this.title = title;
        this.mainImage = mainImage;
        this.categories = categories;
    }

    public static Local getFromJson(JSONObject obj) throws JSONException {
        int id = obj.getInt("id");
        int likes = obj.getInt("likes");
        String title = obj.getString("title");
        String mainImage = IMG_TH_URL + obj.getString("path");
        JSONArray categories = obj.getJSONArray("categories");
        int categoriesSize = categories.length();
        List<Category> list = new ArrayList<>();
        for (int i = 0; i < categoriesSize; i++) {
            JSONObject object = categories.getJSONObject(i);
            list.add(Category.getFromJson(object));
        }
        return new Local(id, likes, title, mainImage, list);
    }

    public static Local getFullFromJson(JSONObject obj) throws JSONException {
        Local l = getFromJson(obj);

        l.setDesc(obj.getString("desc"));
        l.setLatLng(new LatLng(obj.getDouble("lat"), obj.getDouble("lng")));

        JSONArray images = obj.getJSONArray("images");
        List<String> list = new ArrayList<>();
        int imagesSize = images.length();
        for (int i = 0; i < imagesSize; i++) {
            list.add(images.getString(i));
        }
        l.setImages(list);

        String phone = obj.getString("phone");
        String website = obj.getString("website");
        String email = obj.getString("email");
        String address = obj.getString("address");
        l.setContact(new Contact(phone, website, email, address));

        List<Related> relatedList = new ArrayList<>();
        JSONObject relatedObj = obj.getJSONObject("related");
        JSONArray relatedLocals = relatedObj.getJSONArray("locals");
        int relatedLocalsSize = relatedLocals.length();
        for (int i = 0; i < relatedLocalsSize; i++) {
            JSONObject objLocal = relatedLocals.getJSONObject(i);
            relatedList.add(Related.getFromJson(Related.KEY_LOCAL, objLocal));
        }
        JSONArray relatedPersonalities = relatedObj.getJSONArray("personalities");
        int relatedPersonalitiesSize = relatedPersonalities.length();
        for (int i = 0; i < relatedPersonalitiesSize; i++) {
            JSONObject objPersonality = relatedPersonalities.getJSONObject(i);
            relatedList.add(Related.getFromJson(Related.KEY_PERSONALITY, objPersonality));
        }

        l.setRelated(relatedList);
        return l;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getMainImage() {
        return mainImage;
    }

    public void setMainImage(String mainImage) {
        this.mainImage = mainImage;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public List<Related> getRelated() {
        return related;
    }

    public void setRelated(List<Related> related) {
        this.related = related;
    }
}
