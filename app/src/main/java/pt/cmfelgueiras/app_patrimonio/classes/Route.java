package pt.cmfelgueiras.app_patrimonio.classes;

public class Route {
    private String title, likes, image, km;

    public Route(String title, String likes, String image, String km) {
        this.title = title;
        this.likes = likes;
        this.image = image;
        this.km = km;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getKm() {
        return km;
    }

    public void setKm(String km) {
        this.km = km;
    }
}
