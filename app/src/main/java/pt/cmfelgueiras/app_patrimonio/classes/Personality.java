package pt.cmfelgueiras.app_patrimonio.classes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static pt.cmfelgueiras.app_patrimonio.tasks.ApiTask.IMG_TH_URL;

public class Personality {
    private int id;
    private String name, date, job, image, desc;
    private List<String> images;
    private List<Related> related;

    private Personality(int id,
                        String name,
                        String date,
                        String job,
                        String image) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.job = job;
        this.image = image;
    }

    public static Personality getFromJson(JSONObject obj) throws JSONException {
        final int id = obj.getInt("id");
        final String name = obj.getString("name");
        final String job = obj.getString("job");
        final String birth = obj.getString("birth");
        final String death = obj.getString("death");
        final String date = birth + " - " + death;
        final String profilePic = IMG_TH_URL + obj.getString("path");
        return new Personality(id, name, date, job, profilePic);
    }

    public static Personality getFullFromJson(JSONObject obj) throws JSONException {
        Personality p = getFromJson(obj);
        p.setDesc(obj.getString("desc"));
        JSONArray images = obj.getJSONArray("images");
        List<String> list = new ArrayList<>();
        int imagesSize = images.length();
        for (int i = 0; i < imagesSize; i++) {
            list.add(images.getString(i));
        }
        p.setImages(list);

        List<Related> relatedList = new ArrayList<>();
        JSONObject related = obj.getJSONObject("related");

        JSONArray relatedPersonalities = related.getJSONArray("personalities");
        int relatedPersonalitiesSize = relatedPersonalities.length();
        for (int i = 0; i < relatedPersonalitiesSize; i++) {
            JSONObject objRelated = relatedPersonalities.getJSONObject(i);
            relatedList.add(Related.getFromJson(Related.KEY_PERSONALITY, objRelated));
        }

        JSONArray relatedLocals = related.getJSONArray("local");
        int relatedLocalsSize = relatedLocals.length();
        for (int i = 0; i < relatedLocalsSize; i++) {
            JSONObject objRelated = relatedLocals.getJSONObject(i);
            relatedList.add(Related.getFromJson(Related.KEY_LOCAL, objRelated));
        }

        p.setRelated(relatedList);
        return p;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public List<Related> getRelated() {
        return related;
    }

    public void setRelated(List<Related> related) {
        this.related = related;
    }

    @Override
    public String toString() {
        return "Personality{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", date='" + date + '\'' +
                ", job='" + job + '\'' +
                ", image='" + image + '\'' +
                ", desc='" + desc + '\'' +
                ", images=" + Arrays.toString(images.toArray()) +
                '}';
    }
}
