package pt.cmfelgueiras.app_patrimonio.utils;

import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import pt.cmfelgueiras.app_patrimonio.R;

public class LoadImagesUtil {

    public static void loadDrawable(int drawable, ImageView image) {
        Picasso.get()
                .load(drawable)
                .error(R.drawable.ic_error_triangle_24dp)
                .placeholder(R.drawable.anim_rotate)
                .into(image);
    }

    public static void loadDrawable(int drawable, ImageView image, int width, int height) {
        Picasso.get()
                .load(drawable)
                .resize(width, height)
                .error(R.drawable.ic_error_triangle_24dp)
                .placeholder(R.drawable.anim_rotate)
                .into(image);
    }

    public static void loadImage(String url, ImageView image) {
        Picasso.get()
                .load(url)
                .error(R.drawable.ic_error_triangle_24dp)
                .placeholder(R.drawable.anim_rotate)
                .into(image);
    }

    public static void loadImageResize(String url, ImageView image, int width, int height) {
        Picasso.get()
                .load(url)
                .resize(width, height)
                .error(R.drawable.ic_error_triangle_24dp)
                .placeholder(R.drawable.anim_rotate)
                .into(image);
    }

    public static void loadImage(String url, ImageView image, int placeholder) {
        Picasso.get()
                .load(url)
                .error(R.drawable.ic_error_triangle_24dp)
                .placeholder(placeholder)
                .into(image);
    }

    public static void loadImage(String url, ImageView image, int placeholder, int error) {
        Picasso.get()
                .load(url)
                .error(error)
                .placeholder(placeholder)
                .into(image);
    }
}
