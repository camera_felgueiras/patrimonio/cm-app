package pt.cmfelgueiras.app_patrimonio.utils;

import android.app.Activity;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.Menu;
import android.view.MenuItem;

import pt.cmfelgueiras.app_patrimonio.R;

public class MenuUtils {

    public static void hideOption(Menu menu, int id) {
        if (menu != null) {
            MenuItem item = menu.findItem(id);
            if (item != null) {
                item.setVisible(false);
            }
        }
    }

    public static void showOption(Menu menu, int id) {
        if (menu != null) {
            MenuItem item = menu.findItem(id);
            if (item != null) {
                item.setVisible(true);
            }
        }
    }

    public static Drawable setIconColor(Activity activity, MenuItem item) {
        Drawable newIcon = DrawableCompat.wrap(item.getIcon());
        ColorStateList colorSelector =
                ResourcesCompat.getColorStateList(
                        activity.getResources(),
                        R.color.white,
                        activity.getTheme());
        DrawableCompat.setTintList(newIcon, colorSelector);
        return newIcon;
    }
}
