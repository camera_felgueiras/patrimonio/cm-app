package pt.cmfelgueiras.app_patrimonio.tasks;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;

public class ApiTask extends AsyncTask<String, JSONObject, JSONObject> {

    private static final String API_URL = "http://felgueirasid.cm-felgueiras.pt/api";
    //private static final String API_URL = "http://172.20.130.21:8080/api";
    public static final String IMG_URL = API_URL + "/image/";
    public static final String IMG_TH_URL = API_URL + "/image/th/";
    private static final int READ_TIMEOUT = 15000;
    private static final int CONNECTION_TIMEOUT = 15000;
    private ApiListener listener;

    public void setListener(ApiListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        listener.onPre();
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);
        listener.onPost(jsonObject);
    }

    @Override
    protected JSONObject doInBackground(String... args) {
        HttpURLConnection connection = null;
        BufferedReader reader = null;
        OutputStream outStream = null;
        InputStream inStream = null;

        try {
            URL url = new URL(API_URL + args[1]);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(args[0]);
            connection.setReadTimeout(READ_TIMEOUT);
            connection.setConnectTimeout(CONNECTION_TIMEOUT);
            connection.setRequestProperty("Content-Type", "application/json;charset=utf-8");

            if (args[0].equals("POST")) {
                byte[] message = args[2].getBytes();
                connection.setFixedLengthStreamingMode(message.length);

                try {
                    outStream = new BufferedOutputStream(connection.getOutputStream());
                    outStream.write(message);
                    outStream.flush();
                } catch (ConnectException e) {
                    return new JSONObject("{ \"error\": 404 }");
                }
            }

            int code = connection.getResponseCode();
            if (code != 200) {
                inStream = connection.getErrorStream();
            } else {
                inStream = connection.getInputStream();
            }

            reader = new BufferedReader(new InputStreamReader(inStream));
            StringBuilder builder = new StringBuilder();
            String line;

            while ((line = reader.readLine()) != null) {
                builder.append(line).append("\n");
            }

            Log.d("log", "doInBackground: " + code);
            JSONObject obj = new JSONObject(builder.toString());
            obj.accumulate("status_code", code);
            return obj;

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }

            try {
                if (reader != null) {
                    reader.close();
                }
                if (outStream != null) {
                    outStream.close();
                }
                if (inStream != null) {
                    inStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        JSONObject nothingObj = new JSONObject();
        try {
            nothingObj.accumulate("status_code", 400);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return nothingObj;
    }

    public interface ApiListener {
        void onPre();

        void onPost(JSONObject res);
    }
}
