package pt.cmfelgueiras.app_patrimonio.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import pt.cmfelgueiras.app_patrimonio.R;

public class DatesAdapter extends RecyclerView.Adapter<DatesAdapter.ViewHolder> {

    private final Context context;
    private final Listener listener;
    private final List<String> list;
    private int row_index = -1;

    public DatesAdapter(Context context, List<String> list, Listener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_dates, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        String s = list.get(position);
        holder.date.setText(s);
        holder.mainLayout.setOnClickListener(v -> {
            row_index = holder.getAdapterPosition();
            listener.onDateClick(position, s);
            notifyDataSetChanged();
        });

        if (row_index == position) {
            holder.date.setTextColor(ContextCompat.getColor(context, R.color.line_dark));
            holder.itemLine.setBackground(
                    ContextCompat.getDrawable(context, R.drawable.line_bg_dark));
        } else {
            holder.date.setTextColor(Color.BLACK);
            holder.itemLine.setBackground(
                    ContextCompat.getDrawable(context, R.drawable.line_bg_light)
            );
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface Listener {
        void onDateClick(int position, String date);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView date;
        LinearLayout mainLayout;
        View itemLine;

        ViewHolder(View view) {
            super(view);
            mainLayout = view.findViewById(R.id.main_layout);
            date = view.findViewById(R.id.date);
            itemLine = view.findViewById(R.id.item_line);
        }
    }
}
