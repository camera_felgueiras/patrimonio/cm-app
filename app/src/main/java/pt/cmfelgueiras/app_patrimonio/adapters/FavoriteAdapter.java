package pt.cmfelgueiras.app_patrimonio.adapters;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Random;

import pt.cmfelgueiras.app_patrimonio.R;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.ViewHolder> {

    private final List<?> list;
    private final FavoriteListener listener;

    public FavoriteAdapter(List<?> list, FavoriteListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_favorite, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        int red = new Random().nextInt(255);
        int green = new Random().nextInt(255);
        int blue = new Random().nextInt(255);
        viewHolder.image.setImageDrawable(new ColorDrawable(Color.rgb(red, green, blue)));
        viewHolder.title.setText((CharSequence) list.get(i));
        viewHolder.type.setText("Rota");
        viewHolder.btnRemove.setOnClickListener(v -> listener.onRemoveClick(v, i));
        viewHolder.card.setOnClickListener(v -> listener.onCardClick(v, i));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface FavoriteListener {
        void onCardClick(View v, int pos);

        void onRemoveClick(View v, int pos);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        View card;
        TextView title, type;
        ImageView image, btnRemove;

        ViewHolder(@NonNull View view) {
            super(view);
            card = view.findViewById(R.id.card);
            image = view.findViewById(R.id.image);
            btnRemove = view.findViewById(R.id.btn_remove);
            title = view.findViewById(R.id.title);
            type = view.findViewById(R.id.type);
        }
    }
}
