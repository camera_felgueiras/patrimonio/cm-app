package pt.cmfelgueiras.app_patrimonio.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import pt.cmfelgueiras.app_patrimonio.R;
import pt.cmfelgueiras.app_patrimonio.classes.Related;
import pt.cmfelgueiras.app_patrimonio.utils.LoadImagesUtil;

public class RelatedAdapter extends RecyclerView.Adapter<RelatedAdapter.ViewHolder> {

    private final List<?> list;
    private final RelatedListener listener;

    public RelatedAdapter(List<?> list, RelatedListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_related, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        Related item = (Related) list.get(i);

        LoadImagesUtil.loadImage(item.getImage(), viewHolder.image);
        viewHolder.title.setText(item.getTitle());

        final String type = item.getType();
        viewHolder.card.setOnClickListener(v -> listener.onCardClick(v, i, type));
        viewHolder.btnLike.setOnClickListener(v -> listener.onLikeClick(v, i, type));
        viewHolder.btnShare.setOnClickListener(v -> listener.onShareClick(v, i, type));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface RelatedListener {
        void onCardClick(View v, int pos, String type);

        void onLikeClick(View v, int pos, String type);

        void onShareClick(View v, int pos, String type);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        View card;
        ImageView image, btnLike, btnShare;
        TextView title;

        ViewHolder(@NonNull View view) {
            super(view);
            card = view.findViewById(R.id.card);
            image = view.findViewById(R.id.image);
            title = view.findViewById(R.id.title);
            btnLike = view.findViewById(R.id.btn_like);
            btnShare = view.findViewById(R.id.btn_share);
        }
    }
}
