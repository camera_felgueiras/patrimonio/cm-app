package pt.cmfelgueiras.app_patrimonio.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Random;

import pt.cmfelgueiras.app_patrimonio.R;

public class TimelineAdapter extends RecyclerView.Adapter<TimelineAdapter.ViewHolder> {

    private final Context context;
    private final Listener listener;
    private final List<String> list;

    public TimelineAdapter(Context context, List<String> list, Listener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_timeline, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        String s = list.get(position);

        int red = new Random().nextInt(255);
        int blue = new Random().nextInt(255);
        int green = new Random().nextInt(255);
        holder.image.setImageDrawable(new ColorDrawable(Color.rgb(red, green, blue)));

        holder.title.setText(s);
        holder.date.setText("20-10-2020");
        holder.description.setText("Quos porro corrupti labore debitis voluptatem Quos porro corru" +
                "pti labore debitis voluptatem  Quos porro corrupti labore debitis voluptat" +
                "em  Quos porro corrupti labore debitis voluptatem ");

        holder.btnLike.setOnClickListener(v -> listener.onLikeClick(position));
        holder.btnShare.setOnClickListener(v -> listener.onShareClick(position));
        holder.card.setOnClickListener(v -> listener.onCardClick(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface Listener {
        void onShareClick(int position);

        void onLikeClick(int position);

        void onCardClick(int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        CardView card;
        ImageView image, btnLike, btnShare;
        TextView title, date, description;

        ViewHolder(View view) {
            super(view);
            card = view.findViewById(R.id.card);
            image = view.findViewById(R.id.image);
            title = view.findViewById(R.id.title);
            date = view.findViewById(R.id.date);
            description = view.findViewById(R.id.description);
            btnLike = view.findViewById(R.id.btn_like);
            btnShare = view.findViewById(R.id.btn_share);
        }
    }
}
