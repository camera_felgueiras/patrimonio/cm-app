package pt.cmfelgueiras.app_patrimonio.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import pt.cmfelgueiras.app_patrimonio.R;
import pt.cmfelgueiras.app_patrimonio.utils.LoadImagesUtil;

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.ViewHolder> {

    private final List<?> list;
    private final PhotoListener listener;

    public PhotoAdapter(List<?> list, PhotoListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_photo, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        String url = (String) list.get(i);
        LoadImagesUtil.loadImage(url, viewHolder.image);
        viewHolder.image.setOnClickListener(v -> listener.onCardClick(v, i));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface PhotoListener {
        void onCardClick(View v, int pos);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;

        ViewHolder(@NonNull View view) {
            super(view);
            image = view.findViewById(R.id.image);
        }
    }
}
