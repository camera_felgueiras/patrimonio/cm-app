package pt.cmfelgueiras.app_patrimonio.adapters;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Random;

import pt.cmfelgueiras.app_patrimonio.R;
import pt.cmfelgueiras.app_patrimonio.classes.Local;
import pt.cmfelgueiras.app_patrimonio.classes.NaturalHeritageClass;
import pt.cmfelgueiras.app_patrimonio.classes.Personality;
import pt.cmfelgueiras.app_patrimonio.classes.Route;
import pt.cmfelgueiras.app_patrimonio.utils.LoadImagesUtil;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    public static final String TYPE_LIST_ROUTE = "TYPE_LIST_ROUTE";
    public static final String TYPE_LIST_TICKETLINE = "TYPE_LIST_TICKETLINE";
    public static final String TYPE_LIST_SIMPLE = "TYPE_LIST_SIMPLE";
    public static final String TYPE_LIST_OLD = "TYPE_LIST_OLD";
    public static final String TYPE_LIST_PERSONALITY = "TYPE_LIST_PERSONALITY";
    public static final String TYPE_LIST_AGENDA = "TYPE_LIST_AGENDA";

    private final List<?> list;
    private final String type;
    private final ListListener listener;

    public RecyclerAdapter(List<?> list, String type, ListListener listener) {
        this.list = list;
        this.type = type;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        int id;
        switch (type) {
            case TYPE_LIST_TICKETLINE:
                id = R.layout.row_ticketline;
                break;
            case TYPE_LIST_PERSONALITY:
                id = R.layout.row_personality;
                break;
            case TYPE_LIST_AGENDA:
                id = R.layout.row_agenda;
                break;
            case TYPE_LIST_SIMPLE:
                id = R.layout.row_simple;
                break;
            case TYPE_LIST_OLD:
                id = R.layout.row_simple;
                break;
            default:
                id = R.layout.row_routes;
                break;
        }
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(id, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        int red = new Random().nextInt(255);
        int green = new Random().nextInt(255);
        int blue = new Random().nextInt(255);
        switch (type) {
            case TYPE_LIST_TICKETLINE:
                viewHolder.image.setImageDrawable(new ColorDrawable(Color.rgb(red, green, blue)));
                viewHolder.title.setText((CharSequence) list.get(i));
                viewHolder.classification.setText("M/6");
                viewHolder.duration.setText("60 min");
                viewHolder.card.setOnClickListener(v -> listener.onCardClick(v, i, null));
                break;
            case TYPE_LIST_PERSONALITY:
                Personality personItem = (Personality) list.get(i);
                LoadImagesUtil.loadImage(personItem.getImage(), viewHolder.image);
                viewHolder.title.setText(personItem.getName());
                viewHolder.card.setOnClickListener(v -> listener.onCardClick(v, i, null));
                viewHolder.dates.setText(personItem.getDate());
                viewHolder.jobs.setText(personItem.getJob());
                break;
            case TYPE_LIST_AGENDA:
                viewHolder.image.setImageDrawable(new ColorDrawable(Color.rgb(red, green, blue)));
                viewHolder.title.setText((CharSequence) list.get(i));
                viewHolder.card.setOnClickListener(v -> listener.onCardClick(v, i, null));
                viewHolder.dates.setText("22, 12, 03, 06, 02");
                break;
            case TYPE_LIST_SIMPLE:
                Local local = (Local) list.get(i);
                LoadImagesUtil.loadImage(local.getMainImage(), viewHolder.image);
                viewHolder.title.setText(local.getTitle());
                viewHolder.card.setOnClickListener(v -> listener.onCardClick(v, i, null));
                String likes = Integer.toString(local.getLikes());
                viewHolder.likes.setText(likes);
                break;
            case TYPE_LIST_OLD:
                NaturalHeritageClass old = (NaturalHeritageClass) list.get(i);
                LoadImagesUtil.loadImage(old.getMainImage(), viewHolder.image);
                viewHolder.title.setText(old.getTitle());
                viewHolder.card.setOnClickListener(v -> listener.onCardClick(v, i, null));
                viewHolder.likes.setText(old.getLikes());
                break;
            default:
                Route itemRoute = (Route) list.get(i);
                viewHolder.title.setText(itemRoute.getTitle());
                LoadImagesUtil.loadImage(itemRoute.getImage(), viewHolder.image);
                viewHolder.likes.setText(itemRoute.getLikes());
                viewHolder.km.setText(itemRoute.getKm());
                viewHolder.card.setOnClickListener(v -> listener.onCardClick(v, i, null));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface ListListener {
        void onCardClick(View v, int i, String type);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        View card;
        TextView title, likes, km, classification, duration, jobs, dates;
        ImageView image;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            card = itemView.findViewById(R.id.card);
            title = itemView.findViewById(R.id.title);
            image = itemView.findViewById(R.id.image);
            switch (type) {
                case TYPE_LIST_TICKETLINE:
                    classification = itemView.findViewById(R.id.classification);
                    duration = itemView.findViewById(R.id.duration);
                    break;
                case TYPE_LIST_PERSONALITY:
                    jobs = itemView.findViewById(R.id.job);
                    dates = itemView.findViewById(R.id.dates);
                    break;
                case TYPE_LIST_AGENDA:
                    dates = itemView.findViewById(R.id.dates);
                    break;
                case TYPE_LIST_SIMPLE:
                    likes = itemView.findViewById(R.id.likes);
                    break;
                case TYPE_LIST_OLD:
                    likes = itemView.findViewById(R.id.likes);
                    break;
                case TYPE_LIST_ROUTE:
                    likes = itemView.findViewById(R.id.likes);
                    km = itemView.findViewById(R.id.km);
                    break;
            }
        }
    }
}
